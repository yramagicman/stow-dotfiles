reposdir="$HOME/Repos"
# TEST=1
if test -z "$1"; then
    while read line; do
        printf "%s\n" "$line"
    done <<-EOF
-b) [B]in
-c) [C]onfig files
-f) [F]iles
-g) [G]it hooks
-h) [H]ome manager
-i) Lib[i]nput
-j) Cron [J]obs
-n) [N]ixos
-o) [O]pt
-r) [R]emote access (ssh)
-s) [S]ystemd
-v) [V]im
-z) [Z]SH
EOF
fi

sudo() { doas $@; }
if [ $TEST ]; then
    sudo() { echo sudo $@; }
    ln() { echo ln $@; }
    rm() { echo rm $@; }
    cp() { echo ln $@; }
    cd() { echo cd $@; }
    git() { echo git $@; }
    make() { echo make $@; }
    chsh() { echo chsh $@; }
    crontab() { echo crontab $@; }
fi

while test "$1"; do
    case $1 in
        -f) {
        for f in root/*;
        do
            case $f in
                *.conf|*.txt|*.sh|*.md|*.h) ;;
                *) [ -f $f ] && ln -sfv "$PWD/$f" "$HOME/.${f#root/}" ;;
            esac
        done

        [ -f "$PWD/config/bash/bashrc" ] && ln -sfv "$PWD/config/bash/bashrc" "$HOME/.bashrc" 
    } ;;
        -c) {
            mkdir -p "$HOME/.config"
            for d in config/*;
            do
                ln -sfv "$PWD/$d" "$HOME/.config/"
            done
        } ;;
        -b) ln -sfv "$PWD/bin" "$HOME/.local/" ;;
        -s) {
            ln -sfv "$PWD/systemd" "$HOME/.local/"
            systemctl --user enable --now battery_notify.path
            systemctl --user enable --now battery_notify.service
            systemctl --user enable --now downloads.path
            systemctl --user enable --now downloads.service
            systemctl --user enable --now battery_file.timer
            systemctl --user enable --now mbsync.timer
            systemctl --user enable --now mu.timer
            systemctl --user enable --now remind.timer
        } ;;
        -z) {
            if [ "$SHELL" != "$(command -v zsh)" ]; then
                chsh -s "$(command -v zsh)"
            fi
        } ;;
        -v) {
            vim="$HOME/.vim"
            nvimcolor="$HOME/.config/nvim/colors"
            nvimafter="$HOME/.config/nvim/after"
            [ -L $vim ] || [ -d $vim ] && rm -rv $vim
            ln -sfv "$PWD/vim" $vim
            [ -L $nvimcolor ] || [ -d $nvimcolor ] && rm -rv $nvimcolor
            ln -sfv "$PWD/vim/colors" $nvimcolor

            [ -L $nvimafter ] || [ -d $nvimafter ] && rm -rv $nvimafter
            ln -sfv "$PWD/vim/after" $nvimafter
        } ;;
        -n) {
            if [ -f '/etc/nixos/configuration.nix' ];
            then
                echo "building configuration.nix"
                echo "backing up configuration.nix in place"
                sudo cp -v /etc/nixos/configuration.nix /etc/nixos/configuration.nix.$(date +"%s").bak
            fi
            echo build nix flake from repo
            (cd $(git rev-parse --show-toplevel) && \
                 sudo nixos-rebuild boot --flake './#' --upgrade-all)
        } ;;
        -j) {
            for f in cron/*
            do
                if [ ${f#cron/} == $USER ]; then
                    crontab ${f#cron/}
                fi
                if [ ${f#cron/} == 'root' ]; then
                    sudo crontab ${f#cron/}
                fi
            done
        } ;;
        -g) {
            for f in githooks/*; do
                ln -sfv "$PWD/$f" "$PWD/.git/hooks/${f#githooks/}"
            done
        } ;;

        -h) {
            hmdir="$HOME/.config/home-manager"
            [ -L $hmdir ] || [ -d $hmdir ] && rm -rv $hmdir
            ln -sfv "$PWD" "$hmdir"
        } ;;
        -o) {
            [ -d "/opt" ] || sudo mkdir /opt
            for f in opt/*; do
                sudo ln -sfv "$PWD/$f" "/opt/${f#opt/}"
            done
        } ;;

        -r) {
            ln -sfv "$PWD/ssh/config" "$HOME/.ssh/config"
        } ;;


        -i) {
            confDir="/etc/X11/xorg.conf.d/"
            [ -d $confDir ] || sudo mkdir $confDir
            sudo ln -sfv "$PWD/30-pointers.conf" "$confDir/"
        } ;;

    esac
    shift
done
