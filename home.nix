{ config, pkgs, lib, host, ... }:
let
  home = "/home/jonathan";
  # emacspkg = if host == "kaylee" then pkgs.emacs29-pgtk else pkgs.emacs;
  emacspkg = pkgs.emacs30-pgtk;
  dotfiles = "${home}/Repos/dots";
  gitPath = "${pkgs.git}/bin/git";
  notify = "${pkgs.libnotify}/bin/notify-send";
  mkStartScript = name: pkgs.writeShellScript "${name}.sh" ''
    PATH="/run/current-system/sw/bin:${pkgs.home-manager}/bin:$PATH"
    export NIX_PATH="nixpkgs=/nix/var/nix/profiles/per-user/root/channels/nixos:nixos-config=/etc/nixos/configuration.nix"
    ( cd ${dotfiles} && ${gitPath} pull ) || ${notify} "Home manager update failed to pull dotfiles"
    ${pkgs.home-manager}/bin/home-manager expire-generations "-7 days"
    nix-collect-garbage --delete-older-than '18d'
    ( cd ${dotfiles} && nix flake update --commit-lock-file )
    ( cd ${dotfiles} && ./init.sh -c )
    find ${home}/Nextcloud/ -type d -exec chmod 755 {} \+ > /dev/null & disown
    ${pkgs.home-manager}/bin/home-manager switch --impure
  '';

  mkBatteryEcho = name: pkgs.writeShellScript "${name}.sh" ''
    set -euo pipefail
    PATH="/run/current-system/sw/bin:${pkgs.home-manager}/bin:$PATH"
    set -e
    [ -f /sys/class/power_supply/BAT0/capacity ] && cat /sys/class/power_supply/BAT0/capacity > /tmp/bat
    [ -f /sys/class/power_supply/BAT0/status ] && cat /sys/class/power_supply/BAT0/status  >> /tmp/bat
    exit 0
  '';

  mkBatteryNotifyScript = name: pkgs.writeShellScript "${name}.sh" ''
    set -euo pipefail
    PATH="/run/current-system/sw/bin:${pkgs.home-manager}/bin:$PATH"
    cap=$(sed 1q /tmp/bat)
    batStatus=$(tail -n1 /tmp/bat)
    if [ "$cap" -lt 20 ] && [ "$batStatus" = 'Discharging' ]; then
        ${notify} 'Battery Level' "$( tr '\n' ' ' < /tmp/bat)"
    fi
  '';

  mkNixChannelScript = name: pkgs.writeShellScript "${name}.sh" ''
    set -eo pipefail
    export PATH="/run/current-system/sw/bin/:/run/wrappers/bin/"
    nix-channel --update nixos
    exit 0
  '';

  mkEmacsUpdateScript = name: pkgs.writeShellScript "${name}.sh" ''
    PATH="/run/current-system/sw/bin:${pkgs.home-manager}/bin:$PATH"
    ${notify} "Updating emacs packages"
    [[ -d  ${home}/.config/emacs/elpa ]] && rm -rf ${home}/.config/emacs/elpa
    [[ -d  ${home}/.local/share/nvim ]] && rm -rf ${home}/.local/share/nvim/lazy/*
    [[ -f  ${home}/.config/nvim/lazy-lock.json ]] && rm -rf ${home}/.config/nvim/lazy-lock.json
    ${emacspkg}/bin/emacsclient --eval '(kill-emacs)'
    ${emacspkg}/bin/emacs --bg-daemon
    exit 0
  '';

  mkProtonScript = name: pkgs.writeShellScript "${name}.sh" ''
    export PATH="/run/current-system/sw/bin:${pkgs.home-manager}/bin:$PATH"
    export XDG_DATA_HOME="${home}/.local/share"
    export PASSWORD_STORE_DIR="$XDG_DATA_HOME/proton-pass"
    [[ -d  $PASSWORD_STORE_DIR ]] || mkdir -p $PASSWORD_STORE_DIR && pass init

    # protonmail-bridge-gui --no-window
  '';

  packages = with pkgs; [
    adwaita-icon-theme
    aisleriot
    alacritty
    bc
    bemenu
    brave
    kdePackages.breeze-icons
    btop
    calibre
    chessx
    # cmus
    digikam
    dmenu
    du-dust
    dunst
    dzen2
    emacspkg
    feh
    ffmpeg_6-full
    firefox
    foot
    fzf
    geany
    gf
    ghostscript
    git
    gnome-tweaks
    gnubg
    grim # wayland screenshot utility
    guvcview
    htop
    hunspell
    hunspellDicts.en_US
    hypridle
    hyprsunset
    isync
    k4dirstat
    libnotify
    libreoffice
    light # screen brightness for wayland
    loupe
    mariadb
    moreutils
    mpv
    # msmtp.binaries
    mutt
    nextcloud-client
    nil # nix language server
    pandoc
    pass
    pavucontrol
    php84
    php84Packages.composer
    phpactor
    pwgen
    rc
    rclone # dropbox mount
    rustc
    signal-desktop
    silver-searcher
    slurp # wayland screen area selection, used in conjunction with grim
    socat # read sockets, used in bar
    space-cadet-pinball
    sqlite
    stockfish
    swaybg
    tig
    tldr
    tmux
    universal-ctags
    urlscan
    vlc
    w3m
    wl-clipboard
    xclip
    xfce.mousepad
    xfce.thunar
    xfce.thunar-volman
    xmobar
    xorg.xcursorthemes
    xorg.xkill
    xorg.xmessage
    yazi
    yt-dlp
    zathura
    zoom-us
    zoxide
  ];

  # vimFiles = [ "colors" "after" "plugin" "autoload" ];
  nvimFiles = [ "colors" "after" ];
  luaFiles = [ "lua" ];
  zshFiles = [ "aliases" "functions" "prompts" "zpkg" ];

  zsh = map
    (f:
      {
        name = "zsh/${f}";
        value = (builtins.listToAttrs [
          { name = "source"; value = "${dotfiles}/config/zsh/${f}"; }
          { name = "target"; value = "${home}/.config/zsh/${f}"; }
          { name = "recursive"; value = true; }
        ]);
      })
    zshFiles;

  nvim = map
    (f:
      {
        name = "nvim/${f}";
        value = (builtins.listToAttrs [
          { name = "source"; value = "${dotfiles}/vim/${f}"; }
          { name = "target"; value = "${home}/.config/nvim/${f}"; }
          { name = "recursive"; value = true; }
        ]);
      })
    nvimFiles;
  nvimLua = map
    (f:
      {
        name = "nvim/${f}";
        value = (builtins.listToAttrs [
          { name = "source"; value = "${dotfiles}/config/nvim/${f}"; }
          { name = "target"; value = "${home}/.config/nvim/${f}"; }
          { name = "recursive"; value = true; }
        ]);
      })
    luaFiles;

  bin = [{
    name = "bin";
    value = (builtins.listToAttrs [
      { name = "source"; value = "${dotfiles}/bin"; }
      { name = "target"; value = "${home}/.local/bin"; }
      { name = "recursive"; value = true; }
    ]);
  }];

in
{

  nixpkgs.config.allowUnfree = true;

  home.username = "jonathan";
  home.homeDirectory = home;
  home.packages = packages;
  dconf = {
    enable = true;
    settings."org/gnome/desktop/interface".color-scheme = "prefer-dark";
  };
  programs.neovim = {
    enable = true;
    extraLuaConfig = builtins.readFile (builtins.toPath "${dotfiles}/config/nvim/init.lua");
  };

  programs.zsh = {
    enable = true;
    completionInit = "";
    initExtra = builtins.readFile (
      builtins.toPath "${dotfiles}/config/zsh/zshrc"
    );
    logoutExtra = builtins.readFile (
      builtins.toPath "${dotfiles}/config/zsh/zlogout"
    );
    loginExtra = builtins.readFile (
      builtins.toPath "${dotfiles}/config/zsh/zlogin"
    );
    dotDir = ".config/zsh";
    envExtra = builtins.readFile (
      builtins.toPath "${dotfiles}/root/zshenv"
    );
  };

  home.file = builtins.listToAttrs (
    builtins.concatLists [
      bin
      nvim
      nvimLua
      zsh
    ]
  );

  systemd.user.services = {
    hm-switch = {
      Unit = {
        Description = "Auto-switch home manager on login";
      };
      Service = {
        Type = "simple";
        ExecStart = "${mkStartScript "hm-switch"}";
      };
    };

    # proton-bridge = {
    #   Unit = {
    #     Description = "Start protonmail-bridge";
    #   };
    #   Service = {
    #     Type = "simple";
    #     ExecStart = "${mkProtonScript "proton-bridge"}";
    #   };
    # };

    emacs-update = {
      Unit = {
        Description = "forcibly update emacs packages";
      };
      Service = {
        Type = "simple";
        ExecStart = "${mkEmacsUpdateScript "emacsupdate"}";
      };
    };

    battery-file = {
      Unit = {
        Description = "Write battery information to file in /tmp";
      };
      Service = {
        Type = "oneshot";
        ExecStart = "${mkBatteryEcho "bat"}";
      };
    };

    nix-channel = {
      Unit = {
        Description = "Update nix channels";
      };
      Service = {
        Type = "oneshot";
        ExecStart = "${mkNixChannelScript  "ncu"}";
      };
    };

    battery-notify = {
      Unit = {
        Description = "Send notification if battery is low";
      };
      Service = {
        Type = "oneshot";
        ExecStart = "${mkBatteryNotifyScript "bat-notify"}";
      };
    };
  };

  systemd.user.paths = {

    battery-notify = {
      Unit = {
        Description = "Send notification if battery is low";
      };
      Path = {
        PathModified = "/tmp/bat";
      };

      Install = {
        WantedBy = [ "timers.target" ];
      };
    };
  };

  systemd.user.timers = {
    hm-switch = {
      Unit = {
        Description = "Example description";
      };

      Install = {
        WantedBy = [ "timers.target" ];
      };
      Timer = {
        OnBootSec = "1m";
        Unit = "hm-switch.service";
      };
    };

    # proton-bridge = {
    #   Unit = {
    #     Description = "Start Proton Bridge";
    #   };

    #   Install = {
    #     WantedBy = [ "timers.target" ];
    #   };
    #   Timer = {
    #     OnBootSec = "1m";
    #     OnCalendar = "hourly";
    #     Unit = "proton-bridge.service";
    #   };
    # };

    emacs-update = {
      Unit = {
        Description = "Example description";
      };

      Install = {
        WantedBy = [ "timers.target" ];
      };
      Timer = {
        OnCalendar = "monthly";
        Unit = "emacs-update.service";
        Persistent = true;
      };
    };

    battery-file = {
      Unit = {
        Description = "Update battery file";
      };

      Install = {
        WantedBy = [ "timers.target" ];
      };
      Timer = {
        OnBootSec = "3m";
        OnUnitActiveSec = "1m";
        Unit = "battery-file.service";
      };
    };

    nix-channel = {
      Unit = {
        Description = "Update Nix Channel";
      };

      Install = {
        WantedBy = [ "timers.target" ];
      };
      Timer = {
        OnBootSec = "10m";
        Unit = "nix-channel.service";
      };
    };

  };
  programs.swaylock.enable = true;

  xdg.userDirs.desktop = "$HOME/";

  # This value determines the Home Manager release that your
  # configuration is compatible with. This helps avoid breakage
  # when a new Home Manager release introduces backwards
  # incompatible changes.
  #
  # You can update Home Manager without changing this value. See
  # the Home Manager release notes for a list of state version
  # changes in each release.
  home.stateVersion = "24.05";

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;
}
