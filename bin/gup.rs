use std::env;
use std::fs::File;
// use std::error::Error;
use std::io::{self, BufRead};
use std::path::Path;
use std::process::Command;
use std::process::Output;

fn main() {
    if let Ok(lines) = read_lines(get_config()) {
        for line in lines {
            if let Ok(unwrapped_line) = line {
                chdir_and_fetch(unwrapped_line.clone());
                let state = check_git_state(unwrapped_line.clone());
                let unwrapped_state = state.unwrap();
                if unwrapped_state == 1 {
                    push(unwrapped_line.clone());
                } else if unwrapped_state == -1 {
                    pull(unwrapped_line.clone());
                } else if unwrapped_state == 0 {
                    println!("{}", unwrapped_line + " is ok");
                } else {
                    pull_push(unwrapped_line.clone());
                }
            }
        }
    }
}

// The output is wrapped in a Result to allow matching on errors
// Returns an Iterator to the Reader of the lines of the file.
fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
where P: AsRef<Path>, {
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}


fn get_config() -> String {
    let config_result = env::var("XDG_CONFIG_HOME");
    if config_result.is_ok() {
        return config_result.unwrap() + "/gits"
    }
    return "".to_string()
}

fn chdir_and_fetch(path: String) -> Option<String> {
    println!("{}", path);
    return is_correct_directory_then(path.clone(), ||{
        let output = Command::new("git")
            .arg("fetch")
            .arg("--all")
            .output()
            .unwrap_or_else(| e | {
                panic!("Failed to execue process {}", e)
            });

        let s = String::from_utf8_lossy(&output.stdout);
        return s.to_string();

    })
}

fn check_git_state(path: String) -> Option<i32> {

    return is_correct_directory_then::<i32>(path.clone(), || {
        let _checkout = Command::new("git")
            .arg("checkout")
            .arg("-q")
            .arg("master")
            .output()
            .unwrap_or_else(| e | {
                panic!("Failed to execue process {}", e)
            });

        let local = Command::new("git")
            .arg("rev-parse")
            .arg("@")
            .output()
            .unwrap_or_else(| e | {
                panic!("Failed to execue process {}", e)
            });

        let remote = Command::new("git")
            .arg("rev-parse")
            .arg("@{u}")
            .output()
            .unwrap_or_else(| e | {
                panic!("Failed to execue process {}", e)
            });

        let base = Command::new("git")
            .arg("merge-base")
            .arg("@")
            .arg("@{u}")
            .output()
            .unwrap_or_else(| e | {
                panic!("Failed to execue process {}", e)
            });

        let local_string = String::from_utf8_lossy(&local.stdout);
        let remote_string = String::from_utf8_lossy(&remote.stdout);
        let base_string = String::from_utf8_lossy(&base.stdout);

        if local_string == remote_string {
            return 0 ;
        } else if base_string == remote_string {
            return 1 ;
        } else if base_string == local_string {
            return -1 ;
        } else {
            return 2 ;
        }

    })
}

fn pull(path: String){
    is_correct_directory_then::<String>(path.clone(), || {
        let pull = Command::new("git")
            .arg("pull")
            .arg("--rebase")
            .arg("--progress")
            .output()
            .unwrap_or_else(| e | {
                panic!("Failed to execue process {}", e)
            });
        print_if(pull);
        path.to_string()
    });
}

fn push(path: String){
    is_correct_directory_then::<String>(path.clone(), || {
        let push = Command::new("git")
            .arg("push")
            .arg("--progress")
            .output()
            .unwrap_or_else(| e | {
                panic!("Failed to execue process {}", e)
            });
        print_if(push);
        path.to_string()
    });
}

fn pull_push(path: String){
    is_correct_directory_then::<String>(path.clone(), || {
        let pull = Command::new("git")
            .arg("pull")
            .output()
            .unwrap_or_else(| e | {
                panic!("Failed to execue process {}", e)
            });
        print_if(pull);
        let pull = Command::new("git")
            .arg("push")
            .output()
            .unwrap_or_else(| e | {
                panic!("Failed to execue process {}", e)
            });
        print_if(pull);
        path.to_string()
    });
}

fn print_if(to_print: Output) {
    let out = String::from_utf8_lossy(&to_print.stdout);
    let err = String::from_utf8_lossy(&to_print.stderr);
    if out != "" {
        println!("{}", out);
    } else {
        println!("{}", err);
    }
}

fn is_correct_directory_then<T>(path: String, callback: impl Fn() -> T) -> Option<T> {

    let did_chdir = env::set_current_dir(path.clone()).is_ok();
    if did_chdir {
        Some( callback() )
    } else {
        None
    }
}
