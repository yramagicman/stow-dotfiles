{
  description = "A very basic flake";
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    home-manager.url = "github:nix-community/home-manager";
    home-manager.inputs.nixpkgs.follows = "nixpkgs";
  };
  outputs = { nixpkgs, home-manager, ... }@inputs:
    let
      system = "x86_64-linux";
      pkgs = nixpkgs.legacyPackages.${system};
      authenticated = "x-systemd.automount,noauto,x-systemd.idle-timeout=60,x-systemd.device-timeout=5s,x-systemd.mount-timeout=5s,uid=1000,gid=100,credentials=/opt/.creds,mfsymlinks";
      anon = "x-systemd.automount,noauto,x-systemd.idle-timeout=60,x-systemd.device-timeout=5s,x-systemd.mount-timeout=5s,uid=1000,gid=100,username=guest,mfsymlinks";
      nfsopts = "x-systemd.automount,noauto,x-systemd.idle-timeout=60,x-systemd.device-timeout=5s,x-systemd.mount-timeout=5s";
    in
    {
      formatter.${system} = nixpkgs.legacyPackages.${system}.nixpkgs-fmt;
      homeConfigurations.jonathan = home-manager.lib.homeManagerConfiguration {
        inherit pkgs;
        # Specify your home configuration modules here, for example,
        # the path to your home.nix.
        modules = [
          ./home.nix
          ./nixos/not-nixos
        ];

        # Optionally use extraSpecialArgs
        # to pass through arguments to home.nix
      };

      homeConfigurations."jonathan@tightpants" = home-manager.lib.homeManagerConfiguration {
        inherit pkgs;

        # Specify your home configuration modules here, for example,
        # the path to your home.nix.
        modules = [
          ./home.nix
          ./nixos/tightpants/packages.nix
          ./nixos/base/aria2.nix
        ];

        extraSpecialArgs = {
          host = "tightpants";
        };
        # Optionally use extraSpecialArgs
        # to pass through arguments to home.nix
      };

      homeConfigurations."jonathan@rivertam" = home-manager.lib.homeManagerConfiguration {
        inherit pkgs;

        # Specify your home configuration modules here, for example,
        # the path to your home.nix.
        modules = [
          ./home.nix
          ./nixos/rivertam/packages.nix
          ./nixos/base/aria2.nix
        ];

        extraSpecialArgs = {
          host = "rivertam";
        };
        # Optionally use extraSpecialArgs
        # to pass through arguments to home.nix
      };

      homeConfigurations."jonathan@kaylee" = home-manager.lib.homeManagerConfiguration {
        inherit pkgs;

        # Specify your home configuration modules here, for example,
        # the path to your home.nix.
        modules = [
          ./home.nix
          ./nixos/kaylee/packages.nix
          ./nixos/base/aria2.nix
          # ./nixos/vlc.nix
        ];

        extraSpecialArgs = {
          host = "kaylee";
        };
        # Optionally use extraSpecialArgs
        # to pass through arguments to home.nix
      };

      nixosConfigurations."tightpants" = nixpkgs.lib.nixosSystem {
        inherit system;
        specialArgs = {
          authenticated = authenticated;
          anon = anon;
          nfsopts = nfsopts;
        };
        modules = [
          ./nixos/base
          ./nixos/tightpants
        ];
      };

      nixosConfigurations."rivertam" = nixpkgs.lib.nixosSystem {
        inherit system;
        specialArgs = {
          authenticated = authenticated;
          anon = anon;
          nfsopts = nfsopts;
        };
        modules = [
          ./nixos/base
          ./nixos/rivertam
        ];
      };

      nixosConfigurations."kaylee" = nixpkgs.lib.nixosSystem {
        inherit system;

        specialArgs = {
          authenticated = authenticated;
          anon = anon;
          nfsopts = nfsopts;
        };
        modules = [
          ./nixos/base
          ./nixos/kaylee
          # ./nixos/virtualization.nix
        ];

      };

      nixosConfigurations."browncoat" = nixpkgs.lib.nixosSystem {
        inherit system;

        specialArgs = {
          authenticated = authenticated;
          anon = anon;
          nfsopts = nfsopts;
        };
        modules = [
          ./nixos/base/header.nix
          ./nixos/base/packages.nix
          ./nixos/base/auto-upgrade.nix
          ./nixos/base/services.nix
          ./nixos/base/users.nix
          ./nixos/browncoat
          ./nixos/base/systemd/store-scrub.nix
          ./nixos/base/systemd/store-status.nix
          ./nixos/base/systemd/store-balance.nix
          ./nixos/base/systemd/nextcloud-snapshot.nix
          ./nixos/base/systemd/pihole-ethtool.nix
        ];
      };

      nixosConfigurations."the-yard" = nixpkgs.lib.nixosSystem {
        inherit system;

        specialArgs = {
          authenticated = authenticated;
          anon = anon;
          nfsopts = nfsopts;
        };
        modules = [
          ./nixos/base/header.nix
          ./nixos/base/packages.nix
          ./nixos/base/auto-upgrade.nix
          ./nixos/base/services.nix
          ./nixos/base/users.nix
          ./nixos/the-yard
          ./nixos/base/systemd/store-scrub.nix
          ./nixos/base/systemd/store-status.nix
          ./nixos/base/systemd/store-balance.nix
          ./nixos/base/systemd/nextcloud-snapshot.nix
        ];
      };
    };
}
