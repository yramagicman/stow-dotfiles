hi clear
syntax reset
set background=dark
let colors_name = "monochrome"

"" comment
hi Boolean ctermfg=248
hi Character ctermfg=248
hi ColorColumn ctermbg=233
hi Comment ctermfg=242
hi Conceal ctermfg=8 ctermbg=0
hi Conditional ctermfg=248
hi Constant ctermfg=248
hi CursorColumn ctermbg=242
hi Cursor ctermfg=9 ctermbg=1
hi CursorLine ctermbg=234 ctermfg=248 cterm=none
hi CursorLineNr ctermfg=244 cterm=none
hi DiffAdd ctermfg=248 ctermbg=234
hi DiffChange ctermbg=234 ctermfg=248
hi DiffDelete ctermbg=16 ctermfg=248
hi DiffText ctermfg=248 ctermbg=234
hi Directory ctermfg=248
hi Error ctermfg=248 ctermbg=234
hi FoldColumn ctermbg=none ctermfg=248
hi Folded ctermbg=0 ctermfg=8
hi Function ctermfg=248
hi Identifier ctermfg=248 cterm=none
hi Ignore ctermfg=0
hi IncSearch cterm=reverse
hi Keyword ctermfg=248
hi LineNr ctermfg=241
hi MatchParen ctermbg=8 ctermfg=248
hi ModeMsg cterm=bold
hi MoreMsg ctermfg=248
hi NonText ctermfg=8
hi Normal ctermfg=248 ctermbg=none
hi Number ctermfg=248
hi Operator ctermfg=248
hi Pmenu ctermfg=248 ctermbg=8
hi PmenuSbar ctermbg=0
hi PmenuSbar ctermbg=7
hi PmenuSel ctermbg=7 ctermfg=0
hi PmenuThumb ctermbg=7
hi PreProc ctermfg=248
hi Question ctermfg=248
hi Search ctermbg=7 ctermfg=0
hi SignColumn ctermfg=248 ctermbg=none
hi Special ctermfg=248
hi SpecialKey ctermfg=248
hi SpellBad ctermbg=52 ctermfg=248
hi SpellCap ctermfg=248 ctermbg=23
hi SpellLocal ctermbg=25
hi SpellRare ctermbg=130
hi Statement ctermfg=248 cterm=none
hi StatusLine ctermfg=7 ctermbg=0 cterm=underline
hi StatusLineNC ctermfg=8 ctermbg=0 cterm=none
hi link StatusLineTerm StatusLine
hi link StatusLineTermNC  StatusLineNC
hi String ctermfg=248
hi TabLine ctermfg=248 ctermbg=0
hi TabLineFill ctermfg=248 ctermbg=0
hi TabLineSel ctermfg=248 ctermbg=0
hi Title ctermfg=248
hi Todo ctermfg=248 ctermbg=0 cterm=bold
hi ToolbarButton ctermfg=0 ctermbg=7
hi ToolbarLine ctermbg=0
hi Type ctermfg=248
hi Underlined ctermfg=248
hi VertSplit ctermfg=0 ctermbg=8
hi Visual ctermbg=240
hi Visual ctermfg=0 ctermbg=7
hi WarningMsg ctermfg=224
hi WildMenu ctermfg=248 ctermbg=0
