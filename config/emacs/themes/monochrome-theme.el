;;; monochrome-theme.el --- Custom face theme for Emacs

;; Copyright (C) 2011-2021 Free Software Foundation, Inc.

;; Author: Kristoffer Grönlund <krig@koru.se>

;; This file is part of GNU Emacs.

;; GNU Emacs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; GNU Emacs is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.

;;; Code:

(deftheme monochrome "Almost no color variation. All this theme does is ensure comments are slightly more dim than code")

(let ((class '((class color) (min-colors 89))))
  (custom-theme-set-faces
   'monochrome
   `(default ((,class (:background "#000000" :foreground "#adadad"))))
   `(cursor ((,class (:background "#656565" :foreground "#ffffff"))))
   ;; Highlighting faces
   `(fringe ((,class (:background "#000000" :foreground "#ffffff"))))

   `(line-number ((,class (:background "#000000" :foreground "#555555"))))
   `(line-number-current-line ((,class (:background "#000000" :foreground "#bbbbbb"))))
   `(highlight ((,class (:background "#222222" :foreground "#ffffff"))))
   `(region ((,class (:background "#888888" :foreground "#000000"))))
   `(secondary-selection ((,class (:background "#212121" :foreground "#ffffff"))))
   `(isearch ((,class (:background "#555555" :foreground "#ffffff"))))
   `(lazy-highlight ((,class (:background "#222222" :foreground "#ffffff"))))
   ;; Mode line faces
   `(mode-line ((,class (:background "#222222" :foreground "#777777"))))
   `(mode-line-inactive ((,class (:background "#000000" :foreground "#555555"))))
   ;; Escape and prompt faces
   `(minibuffer-prompt ((,class (:foreground "#adadad"))))
   `(escape-glyph ((,class (:foreground "#adadad" :weight bold))))
   `(homoglyph ((,class (:foreground "#adadad" :weight normal))))
   ;; Font lock faces
   `(font-lock-builtin-face ((,class (:foreground "#adadad"))))
   `(font-lock-comment-face ((,class (:foreground "#888888"))))
   `(font-lock-constant-face ((,class (:foreground "#adadad"))))
   `(font-lock-function-name-face ((,class (:foreground "#adadad"))))
   `(font-lock-keyword-face ((,class (:foreground "#adadad"))))
   `(font-lock-string-face ((,class (:foreground "#adadad"))))
   `(font-lock-type-face ((,class (:foreground "#adadad" ))))
   `(font-lock-variable-name-face ((,class (:foreground "#adadad"))))
   `(font-lock-warning-face ((,class (:foreground "#adadad"))))
   ;; Button and link faces
   `(link ((,class (:foreground "#adadad" :underline t))))
   `(link-visited ((,class (:foreground "#adadad" :underline t))))
   `(button ((,class (:background "#333333" :foreground "#adadad"))))
   `(header-line ((,class (:background "#303030" :foreground "#adadad"))))

   `(web-mode-html-tag-face ((,class ( :foreground "#adadad"))))

   `(sh-heredoc ((,class (:foreground "#adadad"))))
   `(sh-quoted-exec ((,class (:foreground "#adadad"))))

   ;; `(compilation-error ((,class (:foreground "#adadad"))))
   ;; `(compilation-info ((,class (:foreground "#adadad"))))
   ;; `(compilation-line-number ((,class (:foreground "#adadad"))))
   ;; `(compilation-mode-line-exit ((,class (:foreground "#adadad"))))
   ;; `(compilation-mode-line-fail ((,class (:foreground "#adadad"))))
   ;; `(compilation-mode-line-run ((,class (:foreground "#adadad"))))
   ;; `(compilation-warning ((,class (:foreground "#ff0000"))))
   ;; `(confusingly-reordered ((,class (:foreground "#ff0000"))))

   `(flymake-error ((,class (:foreground "#adadad"))))
   `(flymake-info ((,class (:foreground "#adadad"))))
   `(flymake-line-number ((,class (:foreground "#adadad"))))
   `(flymake-mode-line-exit ((,class (:foreground "#adadad"))))
   `(flymake-mode-line-fail ((,class (:foreground "#adadad"))))
   `(flymake-mode-line-run ((,class (:foreground "#adadad"))))
   `(flymake-warning ((,class (:foreground "#adadad"))))

   `(warning ((,class (:foreground "#777777"))))

   ;; Gnus faces
   `(gnus-group-news-1 ((,class (:weight bold :foreground "#adadad"))))
   `(gnus-group-news-1-low ((,class (:foreground "#adadad"))))
   `(gnus-group-news-2 ((,class (:weight bold :foreground "#adadad"))))
   `(gnus-group-news-2-low ((,class (:foreground "#adadad"))))
   `(gnus-group-news-3 ((,class (:weight bold :foreground "#adadad"))))
   `(gnus-group-news-3-low ((,class (:foreground "#adadad"))))
   `(gnus-group-news-4 ((,class (:weight bold :foreground "#adadad"))))
   `(gnus-group-news-4-low ((,class (:foreground "#adadad"))))
   `(gnus-group-news-5 ((,class (:weight bold :foreground "#adadad"))))
   `(gnus-group-news-5-low ((,class (:foreground "#adadad"))))
   `(gnus-group-news-low ((,class (:foreground "#adadad"))))
   `(gnus-group-mail-1 ((,class (:weight bold :foreground "#adadad"))))
   `(gnus-group-mail-1-low ((,class (:foreground "#adadad"))))
   `(gnus-group-mail-2 ((,class (:weight bold :foreground "#adadad"))))
   `(gnus-group-mail-2-low ((,class (:foreground "#adadad"))))
   `(gnus-group-mail-3 ((,class (:weight bold :foreground "#adadad"))))
   `(gnus-group-mail-3-low ((,class (:foreground "#adadad"))))
   `(gnus-group-mail-low ((,class (:foreground "#adadad"))))
   `(gnus-header-content ((,class (:foreground "#adadad"))))
   `(gnus-header-from ((,class (:weight bold :foreground "#adadad"))))
   `(gnus-header-subject ((,class (:foreground "#adadad"))))
   `(gnus-header-name ((,class (:foreground "#adadad"))))
   `(gnus-header-newsgroups ((,class (:foreground "#adadad"))))
   ;; Message faces
   `(message-header-name ((,class (:foreground "#adadad" :weight bold))))
   `(message-header-cc ((,class (:foreground "#adadad"))))
   `(message-header-other ((,class (:foreground "#adadad"))))
   `(message-header-subject ((,class (:foreground "#adadad"))))
   `(message-header-to ((,class (:foreground "#adadad"))))
   `(message-cited-text ((,class (:foreground "#adadad"))))
   `(message-separator ((,class (:foreground "#adadad" :weight bold))))

   '(eglot-highlight-symbol-face ((t (:inherit normal))))

   '(org-table ((t (:foreground "#adadad"))))
   '(org-table-header ((t (:foreground "#adadad"))))
   '(org-link ((t (:foreground "#adadad"))))
   '(org-headline-done ((t (:foreground "#adadad"))))
   '(org-headline-todo ((t (:foreground "#adadad"))))
   '(org-done ((t (:foreground "#adadad"))))
   '(org-todo ((t (:foreground "#adadad"))))

   '(racket-keyword-argument-face ((t (:foreground "#adadad"))))
   '(racket-selfeval-face ((t (:foreground "#adadad"))))))

(custom-theme-set-variables
 'monochrome
 '(ansi-color-names-vector [
                            "#4e4e4e" ;; black
                            "#ff005f" ;; red
                            "#00d700" ;; green
                            "#ffff00" ;; yellow
                            "#00afff" ;; blue
                            "#af5fff" ;; magenta
                            "#00ffff" ;; cyan
                            "9e9e9e" ;; grey
                            ]))

(provide-theme 'monochrome)

;;; monochrome-theme.el ends here
