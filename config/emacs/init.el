;;; package --- init.el
;;; Commentary:
;;; a comment
;;; Code:

;; Hide mouse interface quickly
(if (fboundp 'menu-bar-mode) (menu-bar-mode -1))
(if (fboundp 'tool-bar-mode) (tool-bar-mode -1))
(if (fboundp 'scroll-bar-mode) (scroll-bar-mode -1))
(if (fboundp 'fringe-mode) (fringe-mode -1))

;; Package initialization
;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
(package-initialize)
(require 'package)

(eval-when-compile
  (require 'seq)
  (require 'subr-x))

(defvar theme 'modus-vivendi)
(defvar home (getenv "HOME"))
(defvar my/default-font-size 110)
(defvar my/default-variable-font-size 110)

(defun my/set-font-faces ()
  (interactive)
  (set-face-attribute 'default nil :font "Fira Code" :height my/default-font-size)

  ;; Set the fixed pitch face
  (set-face-attribute 'fixed-pitch nil :font "Fira Code" :height my/default-font-size)

  ;; Set the variable pitch face
  (set-face-attribute 'variable-pitch nil :font "Cantarell" :height my/default-variable-font-size :weight 'regular))

(defun my/reload ()
  (interactive)
  (load user-init-file))

(defun swaptheme ()
  (interactive)
  (cond ((string-equal (symbol-name theme ) (symbol-name 'monochrome ))
         ((lambda ()
            (disable-theme theme)
            (setq theme 'manoj-dark)
            (enable-theme theme))))
        ((string-equal (symbol-name theme) (symbol-name 'manoj-dark ))
         ((lambda ()
            (disable-theme theme)
            (setq theme 'default)
            (enable-theme theme))))

        ((string-equal (symbol-name theme) (symbol-name 'default ))
         ((lambda ()
            (disable-theme theme)
            (setq theme 'monochrome)
            (enable-theme theme))))))

(defun retab ()
  (interactive)
  (untabify (point-min) (point-max)))

(defun fzgitdir ()
  (interactive)
  (setenv "PWD" default-directory)
  (let ((git-dir
         (car
          (process-lines-ignore-status
           (executable-find "git") "rev-parse" "--show-toplevel"))))
    (if (not (equal "fatal:" (car (split-string git-dir))) )
        (fzf-git-files)
      (fzf-find-file (getenv "PWD")))))

(defun findtags ()
  (interactive)
  (let ((my-tags-file (locate-dominating-file default-directory ".etags")))
    (when my-tags-file
      (message "Loading tags file: %s" my-tags-file)
      (visit-tags-table (concat (locate-dominating-file default-directory ".etags") ".etags")))))

(defun tagjump ()
  (interactive)
  (findtags)
  (evil-jump-to-tag))

(defun vat ()
  (interactive)
  (evil-window-vsplit)
  (ansi-term (executable-find "zsh")))

(defun atv ()
  (interactive)
  (vat))

(defun at ()
  (interactive)
  (ansi-term (executable-find "zsh")))

(defun set-x-faces ()
  (interactive)
  (when (display-graphic-p)
    (global-hl-line-mode 1)
    (menu-bar-mode -1)
    ;; (enable-theme theme)
    )
  (when (not (display-graphic-p))
    (global-hl-line-mode 1)
    (menu-bar-mode -1)
    ;; (disable-theme theme)
    )
  (my/set-font-faces))

(defun save-on-focus-lost ()
  (when (not (frame-focus-state))
    (save-some-buffers 1)))

(defun linum-absolute ()
  (interactive)
  (global-display-line-numbers-mode)
  (setq display-line-numbers-type t)
  (global-display-line-numbers-mode))

(defun linum-relative ()
  (interactive)
  (global-display-line-numbers-mode)
  (setq display-line-numbers-type 'relative)
  (global-display-line-numbers-mode))

(setenv "PATH"  (concat
                 home "/.local/share/node_modules/.bin:"
                 "/opt/homebrew/bin/:"
                 "/opt/homebrew/sbin:"
                 (getenv "PATH")))

(add-to-list 'package-archives
             '("melpa" . "https://melpa.org/packages/"))

;; use-package bootstrap and init
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(eval-when-compile
  (require 'use-package)
  (require 'use-package-ensure))

(setq tramp-default-method "ssh")
(setq warning-suppress-types '((comp)))
(setq inhibit-startup-screen t
      ring-bell-function 'ignore)

(setq use-package-always-ensure t)
(setq custom-file (concat user-emacs-directory "custom.el"))
(setq backup-directory-alist '(("." . "~/.config/emacs/backups"))
      backup-by-copying t    ; Don't delink hardlinks
      version-control t      ; Use version numbers on backups
      delete-old-versions t  ; Automatically delete excess backups
      kept-new-versions 20   ; how many of the newest versions to keep
      kept-old-versions 5    ; and how many of the old
      )

(setq-default indent-tabs-mode nil
              c-default-style "k&r"
              tab-width 4
              c-basic-offset 4)

(set-default 'truncate-lines t)

(setq-default ispell-program-name (executable-find "hunspell"))

(setq create-lockfiles nil)
(setq vc-follow-symlinks t)
(setq backward-delete-char-untabify-method 'hungry)

(when (not (string-equal system-type "darwin"))
  (setq initial-buffer-choice "~/.config/emacs/start.org"))

(setq-default electric-indent-inhibit t)

;; (setq eglot-stay-out-of '(flymake))
;; (setq eglot-managed-mode-hook (list (lambda ()
;;                                       (eldoc-mode -1)
;;                                       (flymake-mode -1)
;;                                       (fringe-mode -1))))

(add-to-list 'load-path (concat user-emacs-directory "lisp"))
(add-to-list 'custom-theme-load-path (concat user-emacs-directory "themes"))
(add-to-list 'exec-path (concat home "/.local/share/node_modules/.bin"))


;; misc. packages
(use-package magit)
(use-package disable-mouse)

(use-package no-littering
  :config
  ;; no-littering doesn't set this by default so we must place
  ;; auto save files in the same path as it uses for sessions
  (setq auto-save-file-name-transforms
        `((".*" ,(no-littering-expand-var-file-name "auto-save/") t))))

(use-package clipetty
  :hook (after-init . global-clipetty-mode))

(use-package clojure-mode)

(use-package cider)

(use-package evil
  :init
  (setq evil-want-integration t
        evil-want-keybinding nil
        evil-vsplit-window-right 1
        evil-split-window-below 1
        evil-want-C-u-scroll t)
  :config
  (evil-mode 1)
  (define-key evil-normal-state-map (kbd "gr") 'xref-find-references)
  (add-hook 'evil-normal-state-entry-hook
            (lambda ()
              (save-some-buffers 1))))
(use-package evil-collection
  :after evil
  :config
  (evil-collection-init))

(use-package evil-escape
  :after evil
  :init
  (setq-default evil-escape-key-sequence "  "
                evil-escape-delay 0.4)
  :config
  (evil-escape-mode 1))

(use-package evil-numbers)

(use-package evil-visualstar
  :config
  (global-evil-visualstar-mode t))

(use-package evil-commentary
  :config
  (evil-commentary-mode 1))

(use-package evil-surround
  :config
  (global-evil-surround-mode 1))

(use-package evil-matchit
  :config
  (setq evilmi-shortcut "%")
  (global-evil-matchit-mode 1))

(use-package general
  :config
  (general-create-definer my-space-def
    ;; :prefix my-space
    :prefix "SPC")

  (my-space-def
    :states '( normal )
    :keymaps 'override
    "a" 'org-todo-list
    "B"  'ibuffer
    "c" 'calendar
    "d"  'dired
    "D"  'dired-other-frame
    "ed"  'eval-defun
    "f"  'find-file
    ","  'evil-switch-to-windows-last-buffer
    "gc" 'magit-checkout
    "gs" 'magit-status
    "C-]" 'tagjump
    "gr" 'xref-find-references
    "ld" 'org-toggle-link-display
    "n"  'flymake-goto-next-error
    "Od" 'org-date-from-calendar
    "o"  'ido-switch-buffer-other-frame
    "s"  'save-buffer
    "v"  'atv
    "t" 'org-set-tags-command
    "w"  'visual-line-mode
    "mi" 'compile
    "mm" 'recompile
    )

  (if (executable-find "fzf")
      (my-space-def
        :states '( normal )
        :keymaps 'override
        "F"  'fzgitdir
        "b"  'ivy-switch-buffer
        )
    (my-space-def
      :states '( normal )
      :keymaps 'override
      "f"  'find-file
      "b"  'ivy-switch-buffer
      ))

  (general-define-key
   :states '(normal visual)
   :keymaps 'override
   "C-a" 'evil-numbers/inc-at-pt
   "C-x" 'evil-numbers/dec-at-pt
   "C-c w" 'toggle-truncate-lines
   "C-+" 'text-scale-increase
   "C--" 'text-scale-decrease
   "C-0" 'text-scale-adjust
   "C-w C-f" 'toggle-frame-fullscreen
   "C-c rc" 'org-roam-capture
   "C-c ra" 'org-roam-alias-add
   "C-c rt" 'org-roam-tag-add
   "C-c b" 'buffer-menu
   "C-c f" 'set-x-faces
   "C-c rr" 'my/reload
   "C-c sb" 'flyspell-buffer
   "C-c sn" 'flyspell-goto-next-error
   "C-c sw" 'flyspell-correct-word-before-point
   "C-c hi" 'list-faces-display
   "C-c C-z" 'suspend-frame
   "C-w t" 'atv
   "C-w m" 'maximize-window))

(use-package fzf)

(use-package company
  :init
  (add-hook 'emacs-lisp-mode-hook 'company-mode)
  :bind (:map company-active-map
              ("C-n" . company-select-next)
              ("C-p" . company-select-previous)
              ("C-y" . company-complete-selection))
  :custom
  (company-minimum-prefix-length 3)
  (company-idle-delay 0.2))

(use-package which-key
  :init (which-key-mode)
  :diminish which-key-mode
  :config
  (setq which-key-idle-delay 1))

(use-package ivy
  :config
  (ivy-mode 1))

(use-package evil-org
  :after org
  :hook (org-mode . (lambda () evil-org-mode))
  :config
  (require 'evil-org-agenda)
  (evil-org-agenda-set-keys))

(use-package org-roam
  :config
  (setq find-file-visit-truename t)
  (setq org-roam-completion-everywhere t)
  (when (not (string-equal system-type "darwin"))
    (setq org-roam-directory (file-truename (concat home "/Nextcloud/org-roam")))))

(use-package org
  :config
  (customize-set-variable 'org-directory (concat home "/Nextcloud/org"))
  (customize-set-variable 'org-agenda-files (list (concat home "/Nextcloud/org")
                                                  (concat home "/Nextcloud/org-roam")))
  (setq org-todo-keywords
        '((sequence "TODO" "POSTPONED" "|" "DONE" "CANCELED")))
  :hook
  (org-mode . (lambda ()
                (visual-line-mode))))

(use-package lua-mode
  :mode "\\.lua\\'")

;; Modes
(use-package markdown-mode
  :mode "\\.md\\'"
  :hook (markdown-mode . (lambda ()
                           (visual-line-mode))))

(use-package php-mode
  :mode "\\.\\(php\\|inc\\|tpl\\)\\'"
  :interpreter "php"
  :hook (;(php-mode . eglot-ensure)
         (php-mode . company-mode)
         (php-mode . (lambda nil (flymake-mode nil)))))

(use-package racket-mode
  :mode "\\.rkt\\'"
  :interpreter "racket"
  :hook ((racket-mode . company-mode)))

(use-package web-mode
  :hook ((web-mode . company-mode))
  :mode "\\.\\(html\\|vue\\|blade.php\\)\\'"
  :init
  (setq web-mode-style-padding 0
        web-mode-script-padding 0))

;; ;; (require 'eglot)
;; ;; (require 'web-mode)
;; (define-derived-mode genehack-vue-mode web-mode "ghVue"
;;   "A major mode derived from web-mode, for editing .vue files with LSP support.")
;; (add-to-list 'auto-mode-alist '("\\.vue\\'" . genehack-vue-mode))
;; ;; (add-hook 'genehack-vue-mode-hook #'eglot-ensure)

;; (with-eval-after-load 'eglot
;;   (add-to-list 'eglot-server-programs '(genehack-vue-mode "vls")))

(use-package haskell-mode
  :mode "\\.hs\\'")

(use-package yaml-mode
  :mode "\\.yaml\\'")

(use-package typescript-mode
  :hook ((typescript-mode . company-mode))
  :mode "\\.\\(ts\\|js\\)\\'")

(use-package nroff-mode
  :mode "\\.mom\\'")

(use-package rust-mode
  :mode "\\.rs\\'")

(use-package nix-mode
  :hook ((nix-mode . company-mode))
  :mode "\\.nix\\'")

(use-package dockerfile-mode
  :mode "\\Dockerfile|dockerfile\\'")

(use-package ag)

(use-package yasnippet
  :config
  (setq yas-snippet-dirs
        '("~/Repos/dots/config/emacs/snippets")))

;; ;; ***** modeline *****
(setq-default mode-line-format (list
                                " "
                                mode-line-client
                                '(:eval
                                  (propertize
                                   (cond ((eq 'emacs evil-state) " E " )
                                         ((eq 'normal evil-state) " N ")
                                         ((eq 'visual evil-state) " V ")
                                         ((eq 'insert evil-state) " I ")
                                         (t " U " )
                                         )))
                                " %*%* "
                                vc-mode
                                " %m "
                                '(:eval
                                  (propertize
                                   (if (buffer-file-name)
                                       (mapconcat 'identity (nthcdr 4 (split-string (buffer-file-name) "/")) "/")
                                     " ")))
                                " "
                                mode-line-percent-position
                                " "
                                " L:%0l C:%0c "
                                " "
                                mode-line-modes
                                " "
                                ))
(force-mode-line-update)

;; hooks
(add-hook 'after-init-hook
          (lambda ()
            (load "server")
            (unless (server-running-p) (server-start))))

(add-hook 'makefile-mode-hook 'indent-tabs-mode)
(add-hook 'before-save-hook 'delete-trailing-whitespace)
;; (add-hook 'before-save-hook (lambda nil "" (untabify (point-min) (point-max))))

(add-hook 'kill-buffer-hook (lambda nil ""
                              (when (file-directory-p "~/intelephense")
                                (delete-directory "~/intelephense"))))

(add-hook 'minibuffer-exit-hook (lambda ()
                                  (save-some-buffers 1)))
(defun face-for-frame (frame)
  (with-selected-frame frame
    (my/set-font-faces)))

(defun full-screen-mac ()
  (when (string-equal system-type "darwin")
    (toggle-frame-fullscreen)))

(if (daemonp)
    (add-hook 'after-make-frame-functions 'face-for-frame))

(add-hook 'window-setup-hook 'full-screen-mac)

(add-function :after after-focus-change-function #'save-on-focus-lost)
(add-function :after after-focus-change-function #'evil-normal-state)
(add-function :after after-focus-change-function #'set-x-faces)

(add-hook 'after-init-hook (lambda nil
                             (org-todo-list)
                             (get-buffer "*Org Agenda*")))

(global-set-key (kbd "<escape>") 'keyboard-escape-quit)
(global-set-key (kbd "s-u" ) 'revert-buffer-quick)
(global-set-key (kbd "C-c u u" ) 'revert-buffer-quick)
(global-set-key (kbd "C-c s t" ) 'swaptheme)

(global-set-key (kbd "C-c d t" ) (lambda nil
                                   (interactive)
                                   (disable-theme theme)))

(global-set-key (kbd "C-c e t" ) (lambda nil
                                   (interactive)
                                   (enable-theme theme)))

(electric-indent-mode nil)
(fringe-mode nil)
(global-auto-revert-mode t)
(global-hl-line-mode t)
(linum-relative)
(load custom-file)
(load-theme 'manoj-dark t t)
(load-theme theme t)
(set-x-faces)
(show-paren-mode 1)

(provide 'init.el)
;; init.el ends here
