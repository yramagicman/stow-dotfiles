vim.cmd.filetype('plugin indent on')
vim.cmd.syntax('on')
vim.g.mapleader=" "

local home=os.getenv("HOME")

local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
    vim.fn.system({
        "git",
        "clone",
        "--filter=blob:none",
        "https://github.com/folke/lazy.nvim.git",
        "--branch=stable", -- latest stable release
        lazypath,
    })
end
vim.opt.rtp:prepend(lazypath)

require("lazy").setup({
    'bronson/vim-visual-star-search',
    'editorconfig/editorconfig-vim',
    'junegunn/fzf',
    'junegunn/fzf.vim',
    'sheerun/vim-polyglot',
    'tpope/vim-commentary',
    'tpope/vim-fugitive',
    'tpope/vim-repeat',
    'tpope/vim-surround',
    'vim-scripts/vim-indent-object',
    -- port to lua?
    { url = "https://gitlab.com/yramagicman/auto-omnicomplete.git/" },
})

vim.cmd[[ let g:AutoOmniComplete_complete_map = "\<c-x>\<c-n>" ]]

function Maximize()
    vim.cmd[[ wincmd _ ]]
    vim.cmd[[ wincmd | ]]
end

function split (inputstr, sep)
    if sep == nil then
        sep = "%s"
    end
    local t={}
    for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
        table.insert(t, str)
    end
    return t
end

function HlSearchCallback(timer)
    vim.o.hlsearch = false
end

function Hlsearch()
    vim.o.hlsearch = true
    if vim.g.search_timer then
        vim.fn.timer_stop(vim.g.search_timer)
    end
    vim.g.search_timer = vim.fn.timer_start(1500, HlSearchCallback)
end

function exists(name)
    if type(name)~="string" then return false end
    return os.rename(name,name) and true or false
end

function findtags()
    vim.cmd("silent! set tags=''");
    local path = vim.fn.expand('%:p')
    local path = split( path, '/' )
    path[#path] = nil
    function recurse(p)
        if #p == 0 then
            return
        end
        if #p > 0 then
            local tagspath ='/'.. table.concat(p,'/') .. '/.tags'
            local tagsfile = exists(tagspath)
            if tagsfile then
                vim.cmd( "set tags=" .. tagspath )
                return
            end
        end
        p[#p] = nil
        return recurse(p)
    end
    recurse(path)
end

function LineEndings()
    vim.cmd('update')
    vim.cmd('e ++ff=dos')
    vim.cmd('setlocal ff=unix')
    vim.cmd(':w')
end

function scratch()
    local tmp= vim.fn.system('mktemp')
    print(tmp)
    vim.cmd( ":botright edit " .. tmp )
end

function save()
    if vim.fn.filewritable(vim.fn.expand('%')) == 1 or not vim.fn.findfile(vim.fn.expand('%:t'), vim.fn.expand('%:h')) then
        vim.cmd [[ silent! w! ]]
    end
end

function mkdir (dirname)
    os.execute("mkdir -p" .. dirname)
end

vim.cmd('colorscheme portable')
vim.o.shell = 'zsh'
vim.o.updatetime = 2000
vim.o.backspace = 'indent,eol,start'
vim.o.showcmd = true
vim.o.joinspaces = false
vim.o.startofline = true
vim.o.path = '-/usr/include'
vim.o.mouse = 'a'

vim.o.omnifunc = 'syntaxcomplete#Complete'
vim.o.completeopt = 'menu,menuone,noinsert,noselect'
vim.o.wildmenu = true
vim.o.wildmode = 'longest:full,full'

-- auto-reload modified files
vim.o.autoread = true
-- write files on buffer switch, and other actions
vim.o.autowrite = true

vim.o.redrawtime = 5000
vim.o.lazyredraw = true
vim.o.ttyfast = true
vim.o.number = true
vim.o.relativenumber = true
vim.o.expandtab = true
vim.o.shiftwidth = 4
vim.o.softtabstop = 4
vim.o.autoindent = true
vim.o.foldcolumn = '0'
vim.o.foldmethod = 'indent'
vim.o.foldlevel = 999
vim.o.wrap = true
vim.o.linebreak = true
vim.o.sidescrolloff = 15
vim.o.scrolloff = 2
vim.o.listchars ='tab:▸ ,trail:·,eol:¬,extends:❯,precedes:❮'

vim.o.showbreak = '…→'

vim.o.wrapscan = true
vim.o.incsearch = true
vim.o.ignorecase = true
vim.o.smartcase = true
vim.o.shortmess = 'AIOTWaot'
vim.o.visualbell = true
vim.o.belloff = 'backspace'
if vim.o.diff then
    vim.keymap.set('n', '<C-q>', 'ZZZZ<cr>', { noremap = true })
    vim.o.foldmethod ='diff'
    vim.o.list = true
    vim.o.wrap = false
end
if vim.o.ttimeoutlen == -1 then
    vim.o.ttimeout = true
    vim.o.ttimeoutlen = 100
end
vim.o.breakindent = true
vim.o.backupdir = home .. '/.cache/nvim/backup//'
if not vim.fn.isdirectory(vim.o.backupdir) then
    mkdir(vim.o.backupdir)
end
vim.o.directory = home .. '/.cache/nvim/swaps//'
if not vim.fn.isdirectory(vim.o.directory) then
    mkdir(vim.o.directory)
end
vim.o.undolevels = 5000
vim.o.undodir = home .. '/.cache/nvim/undo//'
vim.o.undofile = true
if not vim.fn.isdirectory(vim.o.undodir) then
    mkdir(vim.o.undodir)
end
vim.o.viminfofile = home .. '/.cache/nviminfo'
vim.o.laststatus = 2
vim.cmd('hi def focused ctermbg=2 ctermfg=0')

vim.o.hidden = true
vim.o.splitbelow = true
vim.o.splitright = true
vim.o.winheight = 20
vim.o.winwidth = 100
vim.o.winminheight = 2
vim.o.winminwidth = 1
vim.o.modeline = true
vim.o.modelines = 3
vim.o.grepprg = 'ag --vimgrep'

vim.keymap.set('i', '<C-g>', '<Esc>')
vim.keymap.set('x', '<C-g>', '<Esc>')
vim.keymap.set('v', '<C-g>', '<Esc>')
vim.keymap.set('c', '<C-g>', '<Esc>')

vim.keymap.set('i', '<leader><leader>', '<Esc>')
vim.keymap.set('x', '<leader><leader>', '<Esc>')
vim.keymap.set('v', '<leader><leader>', '<Esc>')
vim.keymap.set('n', '<leader>s', save)
vim.keymap.set('n', '<leader><leader>', save)
vim.keymap.set('n', '.<leader>', 'i<leader><Esc>')

vim.keymap.set('t', '<leader><leader>', '<C-\\><C-N>')
vim.keymap.set('t', '::', '<C-w><C-N>:')
vim.keymap.set('n', '<C-w>t', ':vert term<cr>')
vim.keymap.set('n', '<leader>t', ':vert term<cr>')

vim.keymap.set('n', '<leader>;', ':set hlsearch!<cr>')
vim.keymap.set('n', '<leader>,', '<C-^>')
vim.keymap.set('n', '<leader>mm', ':wall<cr>:make<cr>')
vim.keymap.set('n', '*', ':lua Hlsearch()<cr>*zz')
vim.keymap.set('n', '#', ':lua Hlsearch()<cr>#zz')
vim.keymap.set('n', '/', ':lua Hlsearch()<cr>/')
vim.keymap.set('n', 'n', ':lua Hlsearch()<cr>nzz')
vim.keymap.set('n', 'N', ':lua Hlsearch()<cr>Nzz')
vim.keymap.set('n', '<C-w>m', ':lua Maximize()<cr>')
vim.keymap.set('n', '<leader>r', ':syn sync fromstart<cr>')
vim.keymap.set('n', '>>', 'V>gv')
vim.keymap.set('n', '<<', 'V<gv')
vim.keymap.set('n', '<leader>e', ':e **/*')
vim.keymap.set('n', '<leader>f', ':Files<cr>')
vim.keymap.set('n', '<leader>b', ':Buffers<cr>')
vim.keymap.set('c', ',e', 'e **/*')
vim.keymap.set('c', ',f', 'find **/*')
vim.keymap.set('c', ',b', 'ls<cr>:b')
vim.keymap.set('c', 'e,', 'e **/*')
vim.keymap.set('c', 'f,', 'find **/*')
vim.keymap.set('c', 'b,', 'ls<cr>:b ')
vim.keymap.set('x', '>', '>gv')
vim.keymap.set('x', '<', '<gv')
vim.keymap.set('c', '<C-a>', '<Home>')
vim.keymap.set('c', '<C-b>', '<Left>')
vim.keymap.set('c', '<C-f>', '<Right>')
vim.keymap.set('c', '<C-d>', '<Delete>')
vim.keymap.set('c', '<Esc>b', '<S-Left>')
vim.keymap.set('c', '<Esc>f', '<S-Right>')
vim.keymap.set('c', '<Esc>d', '<S-right><Delete>')
vim.keymap.set('c', '<C-e>', '<End>')
vim.keymap.set('n', 'gs', '<nop>')
vim.keymap.set('n', '<leader>gs', ':Git<cr>')

local mygroup = vim.api.nvim_create_augroup('vimrc', { clear = true })

vim.api.nvim_create_autocmd({ 'TextYankPost' }, {
    group = mygroup,
    pattern = '*',
    callback = function()
        vim.highlight.on_yank({
            higroup = 'IncSearch',
            timeout = 180,
        })
    end,
})

vim.api.nvim_create_autocmd({
    'BufEnter',
    'CursorHold',
    'BufWritePre',
    'InsertLeave'
}, {
    pattern = '*',
    group = mygroup,
    command = 'checktime',
})
vim.api.nvim_create_autocmd({'BufWritePre','InsertLeave' }, {
    pattern = '*',
    group = mygroup,
    command = "%s/\\s\\+$//e",
})

vim.api.nvim_create_autocmd ({ 'BufEnter' }, {
    pattern = '*',
    group = mygroup,
    command = ':call execute("cd " . FugitiveWorkTree())'
})

vim.api.nvim_create_autocmd({'BufWritePre'}, {
    pattern = '*',
    group = mygroup,
    command = "silent! %s#\\($\\n\\s*\\)\\+\\%$##",
})

vim.api.nvim_create_autocmd({'BufWritePre','InsertLeave' }, {
    pattern = '*',
    group = mygroup,
    command = 'silent! :retab!',
})

vim.api.nvim_create_autocmd({'BufEnter' }, {
    pattern = '*',
    group = mygroup,
    command = 'set cursorline',
})

vim.api.nvim_create_autocmd({'BufLeave' }, {
    pattern = '*',
    group = mygroup,
    command = 'set nocursorline',
})

vim.api.nvim_create_autocmd({'BufLeave' }, {
    pattern = '*',
    group = mygroup,
    callback = function()
        save()
    end
})
vim.api.nvim_create_autocmd({'Filetype' }, {
    pattern = '*',
    group = mygroup,
    command = 'set textwidth=80',
})

vim.api.nvim_create_autocmd({'Filetype' }, {
    pattern = 'mail',
    group = mygroup,
    command = 'set textwidth=0',
})

vim.api.nvim_create_autocmd({'InsertLeave','CursorHold'}, {
    pattern = '*',
    group = mygroup,
    callback = save
})

vim.api.nvim_create_autocmd({'BufEnter'}, {
    pattern = '*',
    group = mygroup,
    callback = findtags
})
