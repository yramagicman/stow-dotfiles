local home=os.getenv("HOME")

-- Set <space> as the leader key
-- See `:help mapleader`
--  NOTE: Must happen before plugins are loaded (otherwise wrong leader will be used)
vim.g.mapleader=" "

vim.cmd[[ let g:AutoOmniComplete_complete_map = "\<c-x>\<c-o>" ]]
-- Set to true if you have a Nerd Font installed and selected in the terminal
vim.g.have_nerd_font = false
vim.g.editorconfig = false

require 'plugins.01_functions'
require 'plugins.02_settings'
require 'plugins.03_keymaps'
require 'plugins.04_autocmds'
-- [[ Install `lazy.nvim` plugin manager ]]
--    See `:help lazy.nvim.txt` or https://github.com/folke/lazy.nvim for more info
local lazypath = vim.fn.stdpath 'data' .. '/lazy/lazy.nvim'
if not vim.loop.fs_stat(lazypath) then
    local lazyrepo = 'https://github.com/folke/lazy.nvim.git'
    vim.fn.system { 'git', 'clone', '--filter=blob:none', '--branch=stable', lazyrepo, lazypath }
end ---@diagnostic disable-next-line: undefined-field
vim.opt.rtp:prepend(lazypath)

-- [[ Configure and install plugins ]]
--
--  To check the current status of your plugins, run
--    :Lazy
--
--  You can press `?` in this menu for help. Use `:q` to close the window
--
--  To update plugins you can run
--    :Lazy update
--
-- NOTE: Here is where you install your plugins.
require('lazy').setup({
    'bronson/vim-visual-star-search',
    'tpope/vim-commentary',
    'tpope/vim-repeat',
    'tpope/vim-surround',
    -- 'sheerun/vim-polyglot',
    'vim-scripts/vim-indent-object',
    { import = 'plugins.lazy' },
})
