return {
    {
        'tpope/vim-fugitive',
        config = function()

            vim.api.nvim_create_autocmd ({ 'BufEnter' }, {
                pattern = '*',
                group = mygroup,
                command = ':call execute("cd " . FugitiveWorkTree())'
            })
        end,
    },

    -- NOTE: Plugins can specify dependencies.
    --
    -- The dependencies are proper plugin specifications as well - anything
    -- you do for a plugin at the top level, you can do for a dependency.
    --
    -- Use the `dependencies` key to specify the dependencies of a particular plugin

    { -- Fuzzy Finder (files, lsp, etc)
        'junegunn/fzf.vim',
        dependencies = {
            'junegunn/fzf',
        },
        config = function()
            vim.keymap.set('n', '<leader>f', ':Files<cr>')
            vim.keymap.set('n', '<leader>b', ':Buffers<cr>')
        end,
    },
    {
        "miikanissi/modus-themes.nvim",
        priority = 1000,
        config = function()
            vim.cmd.colorscheme 'modus'
        end,
    },
}
