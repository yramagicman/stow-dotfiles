local home=os.getenv("HOME")
-- vim.cmd.colorscheme 'wildcharm'
vim.o.termguicolors = false
vim.o.shell = 'zsh'
vim.o.shellcmdflag = '-ic'
vim.o.updatetime = 2000
vim.o.backspace = 'indent,eol,start'
vim.o.showcmd = true
vim.o.joinspaces = false
vim.o.startofline = true
vim.o.path = '-/usr/include'
vim.o.mouse = 'a'

vim.o.omnifunc = 'syntaxcomplete#Complete'
vim.o.completeopt = 'longest,menu,menuone,noinsert,noselect'
vim.o.wildmenu = true
vim.o.wildmode = 'longest:full,full'

-- auto-reload modified files
vim.o.autoread = true
-- write files on buffer switch, and other actions
vim.o.autowrite = true

vim.o.redrawtime = 5000
vim.o.lazyredraw = true
vim.o.ttyfast = true
vim.o.number = true
vim.o.relativenumber = true
vim.o.expandtab = true
vim.o.shiftwidth = 4
vim.o.softtabstop = 4
vim.o.autoindent = true
vim.o.foldcolumn = '0'
vim.o.foldmethod = 'indent'
vim.o.foldlevel = 999
vim.o.wrap = true
vim.o.linebreak = true
vim.o.sidescrolloff = 15
vim.o.scrolloff = 2
vim.o.listchars ='tab:▸ ,trail:·,eol:¬,extends:❯,precedes:❮'

vim.o.showbreak = '…→'

vim.o.wrapscan = false
vim.o.incsearch = true
vim.o.inccommand = 'split'
vim.o.ignorecase = true
vim.o.smartcase = true
vim.o.shortmess = 'AIOTWaot'
vim.o.visualbell = true
vim.o.belloff = 'backspace'
if vim.o.diff then
    vim.keymap.set('n', '<C-q>', 'ZZZZ<cr>', { noremap = true })
    vim.o.foldmethod ='diff'
    vim.o.list = true
    vim.o.wrap = false
end
if vim.o.ttimeoutlen == -1 then
    vim.o.ttimeout = true
    vim.o.ttimeoutlen = 100
end
vim.o.breakindent = true
vim.o.backupdir = home .. '/.cache/nvim/backup//'
if not vim.fn.isdirectory(vim.o.backupdir) then
    mkdir(vim.o.backupdir)
end
vim.o.directory = home .. '/.cache/nvim/swaps//'
if not vim.fn.isdirectory(vim.o.directory) then
    mkdir(vim.o.directory)
end
vim.o.undolevels = 5000
vim.o.undodir = home .. '/.cache/nvim/undo//'
vim.o.undofile = true
if not vim.fn.isdirectory(vim.o.undodir) then
    mkdir(vim.o.undodir)
end
vim.o.viminfofile = home .. '/.cache/nviminfo'
vim.o.laststatus = 2
vim.cmd('hi def focused ctermbg=2 ctermfg=0')

vim.o.hidden = true
vim.o.splitbelow = true
vim.o.splitright = true
vim.o.winheight = 20
vim.o.winwidth = 100
vim.o.winminheight = 2
vim.o.winminwidth = 1
vim.o.modeline = true
vim.o.modelines = 3
vim.o.grepprg = 'ag --vimgrep'
