
local mygroup = vim.api.nvim_create_augroup('vimrc', { clear = true })

vim.api.nvim_create_autocmd({ 'TextYankPost' }, {
    group = mygroup,
    pattern = '*',
    callback = function()
        vim.highlight.on_yank({
            higroup = 'IncSearch',
            timeout = 180,
        })
    end,
})

vim.api.nvim_create_autocmd({
    'BufEnter',
    'CursorHold',
    'BufWritePre',
    'InsertLeave'
}, {
        pattern = '*',
        group = mygroup,
        command = 'checktime',
    })
vim.api.nvim_create_autocmd({'BufWritePre','InsertLeave' }, {
    pattern = '*',
    group = mygroup,
    command = "%s/\\s\\+$//e",
})

vim.api.nvim_create_autocmd({'BufWritePre'}, {
    pattern = '*',
    group = mygroup,
    command = "silent! %s#\\($\\n\\s*\\)\\+\\%$##",
})

vim.api.nvim_create_autocmd({'BufWritePre','InsertLeave' }, {
    pattern = '*',
    group = mygroup,
    command = 'silent! :retab!',
})

vim.api.nvim_create_autocmd({'BufEnter' }, {
    pattern = '*',
    group = mygroup,
    command = 'set cursorline',
})

vim.api.nvim_create_autocmd({'BufLeave' }, {
    pattern = '*',
    group = mygroup,
    command = 'set nocursorline',
})

vim.api.nvim_create_autocmd({'BufLeave' }, {
    pattern = '*',
    group = mygroup,
    callback = save
})
vim.api.nvim_create_autocmd({'Filetype' }, {
    pattern = '*',
    group = mygroup,
    command = 'set textwidth=80',
})

vim.api.nvim_create_autocmd({'Filetype' }, {
    pattern = 'mail',
    group = mygroup,
    command = 'set textwidth=0',
})

vim.api.nvim_create_autocmd({'InsertLeave','CursorHold'}, {
    pattern = '*',
    group = mygroup,
    callback = save
})

-- vim.api.nvim_create_autocmd({'BufEnter'}, {
--     pattern = '*',
--     group = mygroup,
--     callback = findtags
-- })
