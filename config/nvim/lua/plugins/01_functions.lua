function Maximize()
    vim.cmd[[ wincmd _ ]]
    vim.cmd[[ wincmd | ]]
end

local function split (inputstr, sep)
    if sep == nil then
        sep = "%s"
    end
    local t={}
    for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
        table.insert(t, str)
    end
    return t
end

local function HlSearchCallback(_)
    vim.o.hlsearch = false
end

function Hlsearch()
    vim.o.hlsearch = true
    if vim.g.search_timer then
        vim.fn.timer_stop(vim.g.search_timer)
    end
    vim.g.search_timer = vim.fn.timer_start(1500, HlSearchCallback)
end

local function exists(name)
    if type(name)~="string" then return false end
    return os.rename(name,name) and true or false
end

-- function findtags()
--     vim.cmd("silent! set tags=''");
--     local head = vim.fn.expand('%:p')
--     local path = split( head, '/' )
--     path[#path] = nil
--     local function recurse(p)
--         if #p == 0 then
--             return
--         end
--         if #p > 0 then
--             local tagspath ='/'.. table.concat(p,'/') .. '/.tags'
--             local tagsfile = exists(tagspath)
--             if tagsfile then
--                 vim.cmd( "set tags=" .. tagspath )
--                 return
--             end
--         end
--         p[#p] = nil
--         return recurse(p)
--     end
--     recurse(path)
-- end

function LineEndings()
    vim.cmd('update')
    vim.cmd('e ++ff=dos')
    vim.cmd('setlocal ff=unix')
    vim.cmd(':w')
end

-- local function scratch()
--   local tmp= vim.fn.system('mktemp')
--   print(tmp)
--   vim.cmd( ":botright edit " .. tmp )
-- end
--
function save()
    if vim.fn.filewritable(vim.fn.expand('%')) == 1 or not vim.fn.findfile(vim.fn.expand('%:t'), vim.fn.expand('%:h')) then
        vim.cmd [[ silent! w! ]]
    end
end

local function mkdir (dirname)
    os.execute("mkdir -p" .. dirname)
end
return M
