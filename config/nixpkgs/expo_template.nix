{ pkgs ? import <nixpkgs> { } }:

pkgs.mkShell {
  buildInputs = [
    nodejs
    nodePackages.expo-cli
    nodePackages.eas-cli

  ];
}
