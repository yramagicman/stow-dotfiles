{ pkgs ? import <nixpkgs> { } }:
let
  myPhp = pkgs.php83.withExtensions ({ enabled, all }: enabled ++ [
    all.xdebug
  ]);
in
pkgs.mkShell {

  name = "";
  buildInputs = with pkgs; [
    myPhp
    php83Packages.composer
    php83Packages.phpcbf
    php83Packages.phpcs
    nodejs
  ];
  shellHook = ''
    php --version;
    composer --version;
  '';
}
