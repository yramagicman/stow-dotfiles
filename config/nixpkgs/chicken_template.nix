{ pkgs ? import <nixpkgs> { } }:
let
  myPhp = pkgs.php83.withExtensions ({ enabled, all }: enabled ++ [
    all.xdebug
  ]);
in
pkgs.mkShell {

  name = "";
  buildInputs = with pkgs; [
    chicken
    chickenPackages_5.chickenEggs.awful
    chickenPackages_5.chickenEggs.awful-sqlite3
  ];
  shellHook = '' '';
}
