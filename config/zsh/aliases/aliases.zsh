#!/usr/bin/env zsh

# > Easier navigation: .., ..., ...., ....., ~ and -
alias ..="cd .."
alias ...="cd ../.."
alias ....="cd ../../.."
alias .....="cd ../../../.."
alias ~="cd ~"
alias d="dirs -v"

# shortcuts
alias mutt=" mutt -F $HOME/.config/mutt/muttrc"
alias m="mutt"
alias h="history"
alias py="python"
alias j="jobs"
alias mkdir="mkdir -p"

if [[ $INSIDE_EMACS ]]; then
    alias vim="ec"
    alias vi="ec"
    alias v="ec"
    alias v.="ec ."
    alias gvim="ec"
else
    alias nvim="nvim"
    alias vim="nvim"
    alias vi="nvim"
    alias v="nvim"
    alias v.="nvim ."
    alias gnvim="nvim ."
fi
alias vimdiff="nvim -d"
alias oo="nohup xdg-open . > /dev/null &"


function vimrc() {
    vim -c ':e $MYVIMRC'
}

alias t='tmux'
alias :q='exit'
alias bc='bc -l'
alias getmail="$HOME/.local/bin/getallmail"
alias remacs="emacsclient --eval '(kill-emacs)' && emacs --bg-daemon"
if [[ $( command -v doas ) ]]; then
    alias sudo='doas'
    alias doas='doas'
else
    alias sudo='sudo '
fi
alias rm="rm -rv"
alias rsy="rsync -az --progress"

# web dev stuff
alias speedtest="wget -O /dev/null http://speedtest.wdc01.softlayer.com/downloads/test10.zip"
alias wget="wget -c --no-check-certificate"
alias artisan="php artisan"
alias kdb="mysql -u root -h 192.168.1.100 laravel"
alias kdump="mysqldump -u root -h 192.168.1.100 laravel"
function ngi() {
    (builtin cd $HOME/.local/share && npm install "$@")
}

# colorize stuff, utility commands
# Detect which `ls` flavor is in use
if ls --color >/dev/null 2>&1; then # GNU `ls`
    colorflag="--color=auto"
else # OS X `ls`
    colorflag="-G"
fi
if [[ "$INSIDE_EMACS" ]]; then
    colorflag=""
fi

# pretty ls
# List all files colorized in long format
alias l="ls -Flh ${colorflag}"
alias ls-a="ls -Fah ${colorflag}"
# List all files colorized in long format, including dot files
alias la="ls -Flah ${colorflag}"
alias ll="ls -Flh ${colorflag}"
# List only directories
alias lsd='ls -Flh ${colorflag} | grep "^d"'
# Always use color output for `ls`
alias ls="command ls -Fh ${colorflag}"

alias grep="command grep --color=auto"

# volume control
alias maxvol="amixer -c 0 -- set Master 100%"
alias headphones="amixer -c 0 -- set Master 45%"
alias mute="amixer -c 0 -- set Master mute"
alias unmute="amixer -c 0 -- set Master on"

# network
# Gzip-enabled `curl`
alias curl="curl -L --compressed"
# Enhanced WHOIS lookups
alias whois="whois -h whois-servers.net"
alias rip="mdig +short myip.opendns.com @resolver1.opendns.com"
alias ip="ip --color=auto"
function nsync() {
    nextcloudcmd --user jonathan --password $(pass show nextcloud) $HOME/Nextcloud http://100.94.223.34
}

#  utilities
# Ring the terminal bell
alias bell="tput bel;cvlc --play-and-exit $HOME/.config/sounds/beep.mp3 2> /dev/null"
alias wttr="curl wttr.in"
function mpv() {
    [[ -e /usr/bin/mpv ]] && {
        /usr/bin/mpv "$@"
        return
    }
    command mpv "$@"
    return
}

# tmux
alias tkill="tmux kill-session -t"
alias tkills="tmux kill-server"
alias tns="tmux new-session"
alias tls="tmux ls"
function trun() {
    wind_name="cmd_$RANDOM"
    tmux new-window -t "$HOSTNAME" -c "$PWD" -n "$wind_name"
    tmux send-keys -t "$HOSTNAME:$win_name" "cd $PWD && $@ && bell && read && exit" Enter
}

# system management aliases
if  [[ -z $SSH_CLIENT && $(command -v systemctl) ]]; then
    alias poweroff="gup; systemctl poweroff"
    alias reboot="gup; systemctl reboot"
    alias hibernate="systemctl suspend"
elif [[ $(command -v systemctl) ]]; then
    alias poweroff="sudo systemctl poweroff"
    alias reboot="sudo systemctl reboot"
    alias hibernate="sudo systemctl suspend"
else
    alias poweroff="sudo poweroff"
    alias reboot="sudo reboot"
fi
if [[ $(command -v yay) ]]; then
    alias pacman="yay"
fi

# utility commands
if [[ $(command -v nix-shell) ]]; then
    alias sl="nix-shell -p sl --run sl"
fi
alias q="exit"
alias mypw="pwgen -c -n -s -y 26 -1"
alias ndate="date \"+%d-%m-%y\""
# easy reload of zsh stuff
alias rl="exec zsh -l"
alias zconfig="$EDITOR $ZDOTDIR/zshrc"
alias x="startx ~/.config/X11/xinitrc"

# git configs
alias g="git"
alias gca="git commit --all --verbose"
alias gco="git checkout"
alias gs="git status --short"
alias push="git push -u origin HEAD"
alias pull="git pull --rebase"

#  docker
alias docker-compose="podman-compose"
alias dc='docker-compose'
alias drc='docker-compose exec'
alias dupd='docker-compose up -d'
alias dupbd='docker-compose up -d --build'
alias dud='docker-compose down'

# NFS store mounts
alias musicmount="sudo mount -t nfs  browncoat.local:/music /home/jonathan/Music"
alias vidmount="sudo mount -t nfs  browncoat.local:/video /home/jonathan/Videos"
alias storemount="sudo mount -t nfs  browncoat.local:/storage /home/jonathan/Storage"
alias mount.nfs="mount.nfs4"
alias rdrop="(mkdir -p /tmp/dbox) && rclone  mount --vfs-cache-mode full Dropbox: /tmp/dbox & disown && sleep 2 && cd /tmp/dbox"

# BTRFS du/df things
alias bdu="btrfs filesystem du"
alias bdf="btrfs filesystem df"

# tailscale
function tsup-browncoat() {
sudo tailscale up \
    --operator=jonathan \
    --reset \
    --exit-node '100.94.223.34' \
    --exit-node-allow-lan-access=true
}

function tsup-fast() {
sudo tailscale up \
    --operator=jonathan \
    --reset \
    --exit-node '100.87.149.57'
    --exit-node-allow-lan-access=true
}
alias tsdown="sudo tailscale down"
function tsup-exit() {
sudo tailscale up \
    --operator=jonathan \
    --accept-dns=true \
    --advertise-exit-node \
    --accept-routes \
    --advertise-routes=192.168.0.0/24,192.168.1.0/24
}
alias tsk="tailscale status | grep kaylee"
alias tsb="tailscale status | grep browncoat"
alias tst="tailscale status | grep tightpants"

alias debspeed="wget https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/11.5.0-live+nonfree/amd64/iso-hybrid/debian-live-11.5.0-amd64-cinnamon+nonfree.iso -O /dev/null"

alias pihole="sudo docker exec -it pihole pihole"
alias occ="sudo docker exec --user www-data nextcloud php occ"
alias mpv="mpv --profile=fast"
alias nix-generation="sudo nix-env --profile /nix/var/nix/profiles/system/ --list-generations "
alias cl="ollama run codellama"
