#!/bin/sh
xbacklight -set 25
setxkbmap -option compose:menu
setxkbmap -option caps:none

"$HOME/bin/setmouse" &

check_process(){
    if  ! pgrep "$1";
    then
        "$@" &
    fi
}

# test -n  "$( pgrep awesome )" || (  x=1; while [ $x -le $(xrandr | grep ' connected' | wc -l ) ]
# do
#     "$HOME/bin/statusloop" "$x"
#     x=$(( x + 1 ))
# done  ) &

("$HOME/.local/bin/randomwall") &

xset -dpms; xset s off &

(sleep 1s && check_process compton -b)

(sleep 1s && /usr/bin/xscreensaver -no-splash) &
(sleep 5s  && emacs --bg-daemon) &

(sleep 10s && nextcloud --background)
# Set keyboard settings - 250 ms delay and 25 cps (characters per
# second) repeat rate.  Adjust the values according to your
# preferances.
xset r rate 250 25 &

test -n  "$( pgrep awesome )" || dunst &

# Turn on/off system beep
xset b off &

(sleep 45s && check_process redshift) &

#limit the size of dirs history
(
d=$(sort -u  "$HOME/.cache/zsh/dirs" )
rm "$HOME/.cache/zsh/dirs"
echo "$d" > "$HOME/.cache/zsh/dirs"
) &

(
h=$(sort -u  "$HOME/.surf/history.txt" )
rm "$HOME/.surf/history.txt"
echo "$h" > "$HOME/.surf/history.txt"
) &

(
echo "" > "$HOME/.xsession-errors"
if ! stat "$HOME/.xsession-errors.old" > /dev/null; then
    rm "$HOME/.xsession-errors.old"
fi

if  stat "$HOME/.cache/updates" > /dev/null; then
    rm "$HOME/.cache/updates"
fi
) &

(
if test -d /tmp/getmail; then
    rm -rf /tmp/getmail
fi
) &

exit
