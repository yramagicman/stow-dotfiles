{ config, pkgs, ... }:
let
  sharedDirectories = [
    "Documents"
    "Calibre Library"
    "Pictures"
    "Videos"
    "Music"
    "Sites"
  ];
in
{


  services.syncthing = {
    user = "jonathan";
    group = "users";
    enable = true;
    dataDir = "/home/syncthing";
    guiAddress = "0.0.0.0:8384";

    settings = {
      gui = {
        user = "jonathan";
        password = "syncmystuff";
      };

      devices = {
        "kaylee" = { id = "RQZIUDO-R6463VZ-M5SSAUF-M4IYNFZ-HWVSZBL-JCBWNK4-X2WIWVU-KZNFOAR"; };
      };

      folders = builtins.listToAttrs (map
        (f:
          {
            name = "${f}";
            value = (builtins.listToAttrs [
              { name = "path"; value = "${config.users.users.jonathan.home}/${f}"; }
              { name = "devices"; value = [ "kaylee" ]; }
            ]);
          })
        sharedDirectories);
    };
  };

  networking.firewall.allowedTCPPorts = [
    8384
  ];
}
