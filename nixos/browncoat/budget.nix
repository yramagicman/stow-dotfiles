{ config, pkgs, lib, ... }:
{
  system.activationScripts.makePiHoleDir = lib.stringAfter [ "var" ] ''
    mkdir -p /srv/jonathan/budget_scheme/etc/awful
    mkdir -p /srv/jonathan/budget_scheme/var/www/awful
    mkdir -p /srv/jonathan/budget_scheme/var/lib/awful
  '';
  # Containers
  virtualisation.oci-containers.containers."budget-web" = {
    image = "";
    autoStart = true;
    workdir = "/srv/jonathan/budget_scheme";
    volumes = [
      "/srv/jonathan/budget_scheme/etc/awful:/etc/awful:rw"
      "/srv/jonathan/budget_scheme/var/lib/awful:/var/lib/awful:rw"
      "/srv/jonathan/budget_scheme/var/www/awful:/var/www/awful:rw"
    ];
    ports = [
      "9810:9810/tcp"
    ];
    log-driver = "journald";
    extraOptions = [
      "--network-alias=web"
      "--network=budget_default"
    ];
  };

  networking.firewall.allowedTCPPorts = [
    9810
  ];

  networking.firewall.allowedUDPPorts = [
    9810
  ];

}


