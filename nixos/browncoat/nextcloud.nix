{ config, pkgs, ... }:
{
  virtualisation.oci-containers = {
    containers = {
      "nextcloud" = {
        autoStart = true;
        image = "nextcloud:stable";
        ports = [
          "80:80/tcp"
        ];
        volumes = [
          "/srv/storage/nextcloud/data/config/:/var/www/html/config"
          "/srv/storage/nextcloud/data/data/:/var/www/html/data"
        ];

        environment = {
          NEXTCLOUD_ADMIN_USER = "admin";
          NEXTCLOUD_ADMIN_PASSWORD = "q3FXaw2VIYGzAUICVAMRTQ==";
          NEXTCLOUD_TRUSTED_DOMAINS = "100.109.94.104 100.124.185.19 100.125.24.87 100.75.20.127 100.87.66.73 100.94.223.34";

        };
      };
    };
  };

  networking.firewall.allowedTCPPorts = [
    80
  ];
  networking.firewall.allowedUDPPorts = [
    80
  ];
}
