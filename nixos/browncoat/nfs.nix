{ config, pkgs, ... }:
{
  services.nfs.server.enable = true;
  services.rpcbind.enable = true;
  services.nfs.server.exports = ''
    /srv/jonathan 100.125.24.87(rw,sync)
    /srv/jonathan 100.87.66.73(rw,sync)
    /srv/jonathan 100.105.118.72(rw,sync)
    /srv/jonathan 192.168.1.0/24(rw,sync)

    /srv/music 192.168.1.0/24(rw,sync)
    /srv/public 100.125.24.87(rw,sync)
    /srv/public 100.87.66.73(rw,sync)
    /srv/public 100.105.118.72(rw,sync)
    /srv/public 192.168.1.0/24(rw,sync)
    /srv/video 192.168.1.0/24(rw,sync)
  '';

  networking.firewall.allowedTCPPorts = [
    2049
    111
  ];

  networking.firewall.allowedUDPPorts = [
    2049
    111
  ];

}
