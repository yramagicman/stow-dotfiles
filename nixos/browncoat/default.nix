{ config, pkgs, ... }:
{
  imports = [
    ./backup-clean.nix
    ./extras.nix
    ./filesystems.nix
    ./hardware-configuration.nix
    ./jellyfin.nix
    ./network.nix
    ./nextcloud.nix
    ./nfs.nix
    ./pihole.nix
    # ./budget.nix
    ./samba.nix
    ./syncthing.nix
  ];
}
