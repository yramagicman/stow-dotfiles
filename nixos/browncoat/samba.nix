{ config, pkgs, ... }:
{

  services.samba.openFirewall = true;
  services.samba-wsdd.enable = true; # make shares visible for windows 10 clients
  services.samba = {
    enable = true;
    settings =
      {
        global.security = "user";
        global.workgroup = "WORKGROUP";
        global."guest account" = "nobody";
        global."map to guest" = "bad user";

        public = {
          path = "/srv/storage/Public";
          browseable = "yes";
          "read only" = "no";
          "guest ok" = "yes";
          "create mask" = "0644";
          "directory mask" = "0755";
          "force user" = "jonathan";
          "force group" = "users";
          "follow symlinks" = "yes";
        };

        root = {
          path = "/srv/storage/";
          browseable = "yes";
          "read only" = "no";
          "guest ok" = "no";
          "valid users" = "jonathan";
          "create mask" = "0644";
          "directory mask" = "0755";
          "force user" = "jonathan";
          "force group" = "users";
          "follow symlinks" = "yes";
        };

        jonathan = {
          path = "/srv/jonathan/";
          browseable = "yes";
          "read only" = "no";
          "guest ok" = "no";
          "valid users" = "jonathan";
          "create mask" = "0644";
          "directory mask" = "0755";
          "force user" = "jonathan";
          "force group" = "users";
          "follow symlinks" = "yes";
        };

        home = {
          path = "/srv/home/";
          browseable = "yes";
          "read only" = "no";
          "guest ok" = "no";
          "valid users" = "jonathan";
          "create mask" = "0644";
          "directory mask" = "0755";
          "force user" = "jonathan";
          "force group" = "users";
          "follow symlinks" = "yes";
        };

        music = {
          path = "/srv/music/";
          browseable = "yes";
          "read only" = "yes";
          "guest ok" = "yes";
          "create mask" = "0644";
          "directory mask" = "0755";
          "force user" = "jonathan";
          "force group" = "users";
          "follow symlinks" = "yes";
        };

        video = {
          path = "/srv/video/";
          browseable = "yes";
          "read only" = "yes";
          "guest ok" = "yes";
          "create mask" = "0644";
          "directory mask" = "0755";
          "force user" = "jonathan";
          "force group" = "users";
          "follow symlinks" = "yes";
        };

        Emily = {
          path = "/srv/storage/Emily";
          browseable = "yes";
          "read only" = "no";
          "guest ok" = "no";
          "create mask" = "0644";
          "directory mask" = "0755";
        };
      };
  };

  networking.firewall.allowedTCPPorts = [
    5357 # wsdd/samba
  ];

  networking.firewall.allowedUDPPorts = [
    3702 # wsdd/samba
  ];
}
