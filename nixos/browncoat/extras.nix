{ config, pkgs, ... }:
{
  hardware.cpu.intel.updateMicrocode = true;
  services.netdata.enable = true;

  environment.systemPackages = with pkgs; [
    btrfs-progs
    intel-gpu-tools
    podman-compose
    microcodeIntel
    emacs-nox
    pass
    iftop
    fzf
    yt-dlp
    zoxide
  ];

  virtualisation.podman = {
    enable = true;
    autoPrune.enable = true;
    dockerCompat = true;

    # Required for containers under podman-compose to be able to talk to each other.
    # defaultNetwork.settings.dns_enabled = true;
  };

  swapDevices = [
    {
      device = "/swapfile";
      size = 4096;
    }
  ];

  nixpkgs.config.packageOverrides = pkgs: {
    vaapiIntel = pkgs.vaapiIntel.override { enableHybridCodec = true; };
  };

  hardware.graphics = {
    enable = true;
    extraPackages = with pkgs; [
      intel-media-driver # LIBVA_DRIVER_NAME=iHD
      vpl-gpu-rt
      vaapiVdpau
      libvdpau-va-gl
    ];
  };

  security.sudo.extraRules = [
    {
      users = [ "jonathan" "tomg" ];
      commands = [
        { command = "/opt/receive-backup"; options = [ "NOPASSWD" ]; }
        { command = "/opt/dad-backup"; options = [ "NOPASSWD" ]; }
      ];
    }
    {
      users = [ "netdata" ];
      commands = [
        { command = "/run/current-system/sw/bin/smbstatus"; options = [ "NOPASSWD" ]; }
      ];
    }
  ];

  services.apcupsd.enable = true;


  networking.firewall.allowedUDPPorts = [
    10114
  ];

  networking.firewall.allowedTCPPorts = [
    10114
  ];

  users.users.tomg = {
    isNormalUser = true;
    home = "/home/tomg";
    createHome = true;
    extraGroups = [ "audio" "video" "kvm" "wheel" ];
    shell = pkgs.zsh;
    initialPassword = "v/d7xBMgrZnLO[H[W`6z:3Ru@}";
  };

  users.users.emily = {
    isNormalUser = true;
    createHome = false;
    shell = pkgs.shadow;
    group = "users";
  };

  services.printing.enable = true;
  services.printing.browsing = true;
  services.printing.listenAddresses = [ "*:631" ]; # Not 100% sure this is needed and you might want to restrict to the local network
  services.printing.allowFrom = [ "all" ]; # this gives access to anyone on the interface you might want to limit it see the official documentation

  services.locate.enable = false;
  xdg.portal.config.common.default = "*";

}
