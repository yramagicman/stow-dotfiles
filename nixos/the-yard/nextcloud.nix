{ config, pkgs, ... }:
{
  services.nextcloud = {
    enable = false;
    hostName = "the-yard.local";
    home = "/srv/storage/nextcloud/home/";
    datadir = "/srv/storage/nextcloud/data/";
    config.adminpassFile = "${pkgs.writeText "adminpass" "test123"}";
    package = pkgs.nextcloud29;
    # enableBrokenCiphersForSSE = false;

    settings.trusted_domains = [
      "100.109.94.104"
      "100.124.185.19"
      "100.125.24.87"
      "100.75.20.127"
      "100.87.66.73"
      "100.94.223.34"
    ];
  };

}
