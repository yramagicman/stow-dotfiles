{ config, pkgs, ... }:
{
  services.jellyfin.enable = true;
  services.jellyfin.openFirewall = true;
  users.users.jonathan.extraGroups = [ "jellyfin" ];
  users.users.jellyfin.extraGroups = [ "render" ];
}
