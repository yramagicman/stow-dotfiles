{ config, pkgs, ... }:
let
  backup = ''
    #!/usr/bin/env zsh
    declare -a BACKUP_DIRS
    BACKUP_DIRS=(
        "/srv/storage/backup/tightpants"
        "/srv/storage/backup/kaylee"
        "/srv/storage/bakerst/BakerSt"
    )
    for d in $BACKUP_DIRS; do
        for f in {1..$(($(date +"%m") - 2))};
        do
        printf -v FORMATTED "%02d" "$f"
            dir="$d/$FORMATTED"
            [[ -d "$dir" ]] && while [[ $(ls "$dir" | wc -l) -gt 1 ]];
                do
                    sudo btrfs subvolume delete "$dir/$(ls "$dir" | head -n 1)"
                done || echo 'Fail'
            done
        done
  '';
in
{
  systemd.timers = {

    "backup-clean" = {
      wantedBy = [ "timers.target" "network.target" ];
      enable = false;
      timerConfig = {
        Unit = "backup-clean.service";
        OnBootSec = "5m";
      };
    };
  };


  systemd.services = {
    "backup-clean" = {
      serviceConfig.Type = "oneshot";
      description = "Backup home directory to local server";
      script = backup;
      wantedBy = [ "backup-clean.timer" ];
      after = [ "network-online.target" ];
      wants = [ "network-online.target" ];
    };
  };
}
