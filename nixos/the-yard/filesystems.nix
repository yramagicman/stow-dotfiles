{ config, pkgs, ... }:
{
  fileSystems."/mnt/Passport".device = "/dev/disk/by-partlabel/Passport";
  fileSystems."/mnt/Passport".options = [ "compress=zstd" ];


  # STORAGE DRIVE MOUNTED IN nixos/the-yard/cryptopen.nix
  # fileSystems."/srv/music" = {
  #   device = "/home/jonathan/Music";
  #   options = [ "bind" ];
  # };

  # fileSystems."/srv/video" = {
  #   device = "/home/jonathan/Videos";
  #   options = [ "bind" ];
  # };

  fileSystems."/srv/storage" = {
    device = "/mnt/bulk";
    options = [ "bind" ];
  };

  # fileSystems."/srv/jonathan" = {
  #   device = "/srv/storage/jonathan";
  #   options = [ "bind" ];
  # };

  # fileSystems."/srv/public" = {
  #   device = "/srv/storage/Public";
  #   options = [ "bind" ];
  # };

  # fileSystems."/srv/home" = {
  #   device = "/home/jonathan/";
  #   options = [ "bind" ];
  # };

}
