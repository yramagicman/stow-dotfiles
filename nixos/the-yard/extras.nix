{ config, lib, pkgs, ... }:
{
  hardware.cpu.intel.updateMicrocode = true;
  boot.loader.timeout = lib.mkForce 5;
  services.netdata.enable = true;

  environment.systemPackages = with pkgs; [
    acpilight
    btrfs-progs
    emacs-nox
    fzf
    iftop
    intel-gpu-tools
    microcodeIntel
    podman-compose
    vim
  ];

  virtualisation.podman = {
    enable = true;
    autoPrune.enable = true;
    dockerCompat = true;

    # Required for containers under podman-compose to be able to talk to each other.
    # defaultNetwork.settings.dns_enabled = true;
  };

  nixpkgs.config.packageOverrides = pkgs: {
    vaapiIntel = pkgs.vaapiIntel.override { enableHybridCodec = true; };
  };

  hardware.graphics = {
    enable = true;
    extraPackages = with pkgs; [
      intel-media-driver
      libvdpau-va-gl
      vaapiVdpau
    ];
  };

  security.sudo.extraRules = [
    {
      users = [ "jonathan" "tomg" ];
      commands = [
        { command = "/opt/receive-backup"; options = [ "NOPASSWD" ]; }
        { command = "/opt/dad-backup"; options = [ "NOPASSWD" ]; }
      ];
    }
    {
      users = [ "netdata" ];
      commands = [
        { command = "/run/current-system/sw/bin/smbstatus"; options = [ "NOPASSWD" ]; }
      ];
    }
  ];

  users.users.tomg = {
    isNormalUser = true;
    home = "/home/tomg";
    createHome = true;
    extraGroups = [ "audio" "video" "kvm" "wheel" ];
    shell = pkgs.zsh;
    initialPassword = "v/d7xBMgrZnLO[H[W`6z:3Ru@}";
  };

  users.users.emily = {
    isNormalUser = true;
    createHome = false;
    shell = pkgs.shadow;
    group = "users";
  };

  services.printing.browsing = true;
  services.printing.listenAddresses = [ "*:631" ]; # Not 100% sure this is needed and you might want to restrict to the local network
  services.printing.allowFrom = [ "all" ]; # this gives access to anyone on the interface you might want to limit it see the official documentation

  xdg.portal.config.common.default = "*";

}
