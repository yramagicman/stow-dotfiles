{ config, pkgs, ... }:
{

  services.samba.openFirewall = true;
  services.samba-wsdd.enable = true; # make shares visible for windows 10 clients
  services.samba = {
    enable = true;
    securityType = "user";
    extraConfig = ''
      workgroup = WORKGROUP
      server string = smbnix
      netbios name = smbnix
      security = user
      guest account = nobody
      map to guest = bad user
    '';
    shares = {
      storage = {
        path = "/srv/storage/";
        browseable = "yes";
        "read only" = "no";
        "guest ok" = "yes";
        "create mask" = "0644";
        "directory mask" = "0755";
        "force user" = "tomg";
        "force group" = "users";
        "follow symlinks" = "yes";
      };

      Passport = {
        path = "/mnt/Passport/";
        browseable = "yes";
        "read only" = "no";
        "guest ok" = "yes";
        "create mask" = "0644";
        "directory mask" = "0755";
        "force user" = "tomg";
        "force group" = "users";
        "follow symlinks" = "yes";
      };
    };
  };

  networking.firewall.allowedTCPPorts = [
    5357 # wsdd/samba
  ];

  networking.firewall.allowedUDPPorts = [
    3702 # wsdd/samba
  ];
}
