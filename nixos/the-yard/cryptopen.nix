{ config, pkgs, ... }:
let
  backup = ''
    #!/usr/bin/env bash
    set -e
     ${pkgs.cryptsetup}/bin/cryptsetup open /dev/disk/by-uuid/eae86cb6-715a-4746-8b1a-87c3b0cb7a34 crypt-btrfs-eae86cb6 --key-file=/.fde/btrfs-eae86cb6.keyfile
     ${pkgs.cryptsetup}/bin/cryptsetup open /dev/disk/by-uuid/7f88089c-0997-4639-b553-c007c0c19fc6 crypt-btrfs-7f88089c --key-file=/.fde/btrfs-7f88089c.keyfile
     /run/wrappers/bin/mount -o compress=zstd /dev/disk/by-label/external_backup /mnt/bulk
  '';
in
{

  systemd.timers = {
    "cryptopen" = {
      wantedBy = [ "timers.target" ];
      enable = true;
      timerConfig = {
        OnBootSec = "2m";
        Unit = "cryptopen.service";
      };
    };
  };
  systemd.services = {
    "cryptopen" = {
      serviceConfig.Type = "simple";
      enable = true;
      description = "Open encrypted drive";
      script = backup;
      wantedBy = [ "cryptopen.timer" ];
      after = [ "network-online.target" ];
      wants = [ "network-online.target" ];
    };
  };
}
