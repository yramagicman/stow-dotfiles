{ config, pkgs, ... }:
{
  networking.hostName = "the-yard"; # Define your hostname.
  networking.wireless.enable = false; # Enables wireless support via wpa_supplicant.

  networking.useDHCP = true;
  # networking.defaultGateway.address = "192.168.1.1";
  networking.nameservers = [
    "192.168.1.1"
    "1.1.1.1"
    "1.0.0.1"

  ];
  # networking.interfaces.enp4s0.ipv4.addresses = [
  #   {
  #     address = "192.168.1.224";
  #     prefixLength = 24;
  #   }
  # ];

  # networking.interfaces.enp0s31f6.ipv4.addresses = [
  #     {
  #         address = "192.168.1.203";
  #         prefixLength = 24;
  #     }
  # ];

  networking.firewall.allowedTCPPorts = [
    19999 #netdata
    80
    443
    631
  ];

  networking.firewall.allowedUDPPorts = [
    631
  ];


  networking.firewall.allowPing = true;
  services.tailscale.port = 47863;


  services.iperf3.enable = true;
  services.iperf3.verbose = true;
  services.iperf3.openFirewall = true;

}
