{ config, pkgs, ... }:
{
  imports = [
    ./backup-clean.nix
    ./extras.nix
    ./filesystems.nix
    ./hardware-configuration.nix
    ./jellyfin.nix
    ./network.nix
    ./cryptopen.nix
    ./nextcloud.nix
    ./nfs.nix
    ./pihole.nix
    ./rediary.nix
    ./samba.nix
  ];
}
