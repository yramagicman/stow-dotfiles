{ config, pkgs, ... }:
{
  services.nfs.server.enable = true;
  services.rpcbind.enable = true;
  services.nfs.server.exports = ''
    /srv/storage 192.168.0.0/24(rw,sync)
    /mnt/Passport 100.109.94.104(rw,sync)
    /mnt/Passport 100.118.10.88(rw,sync)
    /mnt/Passport 100.100.130.42(rw,sync)
    /mnt/Passport 100.126.233.32(rw,sync)
    /mnt/Passport 100.109.94.104(rw,sync)
    /mnt/Passport 100.86.34.6(rw,sync)
    /mnt/Passport 100.85.50.100(rw,sync)

  '';

  networking.firewall.allowedTCPPorts = [
    2049
    111
  ];

  networking.firewall.allowedUDPPorts = [
    2049
    111
  ];

}
