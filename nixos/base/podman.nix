{ config, pkgs, ... }: {

  environment.systemPackages = with pkgs; [
    podman-compose
  ];

  # Runtime
  virtualisation.oci-containers.backend = "podman";
  virtualisation.podman = {
    enable = true;
    autoPrune.enable = true;
    dockerCompat = true;
    # Required for containers under podman-compose to be able to talk to each other.
    defaultNetwork.settings.dns_enabled = true;
  };
}
