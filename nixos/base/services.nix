{ config, pkgs, lib, ... }:
{

  boot.swraid.enable = false;
  system.activationScripts.makeSmartdDir = lib.stringAfter [ "var" ] '' mkdir -p /var/log/smartd/ '';
  # Enable sound.
  # sound.enable = true;

  hardware.enableRedistributableFirmware = true;
  systemd.network.wait-online.anyInterface = true;

  services = {
    gvfs.enable = true;
    # tlp.enable = true;
    udisks2.enable = true;
    # Enable CUPS to print documents.
    printing.enable = true;
    avahi = {
      enable = true;
      publish = {
        enable = true;
        workstation = true;
        userServices = true;
      };
      openFirewall = true;
      nssmdns4 = true;
    };

    pipewire.enable = true;
    pipewire.pulse.enable = true;
    pipewire.alsa.enable = true;
    pipewire.wireplumber.enable = true;
    fwupd.enable = true;

    # Enable the OpenSSH daemon.
    openssh = {
      enable = true;
      authorizedKeysInHomedir = true;
    };

    netdata.enable = true;

    # httpd.enable = true;
    # httpd.adminAddr = "yramagicman@gmail.com";
    # httpd.enablePHP = true;
    # mysql.package = pkgs.mariadb;
    # mysql.enable = true;

    phpfpm.phpOptions = ''
      memory_limit = 2048M
    '';

    redshift.enable = true;
    geoclue2.enable = true;
    fstrim.enable = true;
    nscd.enableNsncd = true;
    # enable tailscale
    tailscale.enable = true;
    tailscale.interfaceName = "tailscale0";
    gnome.gnome-keyring.enable = true;
    smartd = {
      enable = true;
      extraOptions = [
        "-A /var/log/smartd/"
        "--interval=3600"
      ];
    };

    earlyoom.enable = true;
  };
  networking.firewall.checkReversePath = "loose";
  networking.firewall.allowedTCPPorts = [
    19999
  ];
  security.polkit.enable = true;

  location.provider = "geoclue2";
  # hardware.logitech.wireless.enable = true;
  # hardware.logitech.wireless.enableGraphical = true;
  services.pulseaudio.enable = false;
  programs.wireshark.enable = true;
  programs.wireshark.package = pkgs.wireshark-qt;

  security.rtkit.enable = true;
  xdg.portal.enable = true;

  # This is unfortuantely necessary for swaylock to actually unlock
  security.pam.services.swaylock = { };

  # security.apparmor.enable = true;
  # security.apparmor.packages = [
  #     pkgs.apparmor-profiles
  # ];

  services.rpcbind.enable = true; # nfs

}
