{ config, pkgs, ... }:
{
  fonts.packages = with pkgs; [
    cantarell-fonts
    corefonts
    fira-code
    fira-mono
    noto-fonts
    terminus_font
    winePackages.fonts
  ];



  fonts.fontconfig = {
    # Fixes pixelation
    antialias = true;

    # Fixes antialiasing blur
    hinting = {
      enable = true;
      style = "full"; # no difference
      autohint = true; # no difference
    };

    subpixel = {
      # Makes it bolder
      rgba = "rgb";
      lcdfilter = "default"; # no difference
    };
  };


}
