{ config, pkgs, ... }:
let
  snapScript = ''
    #!/usr/bin/env zsh
    export PATH="/run/current-system/sw/bin/:/run/wrappers/bin/"
    hour="/home/.snapshots/$(date -Ihour)"
    day="/home/.snapshots/$(date -I)"

    [[ -d $day ]] || {
        btrfs subvolume snapshot -r -- "/home" $day
        for hourly in /home/.snapshots/*T*; do
            [[ -d $hourly ]] && btrfs subvolume delete $hourly
        done
    }

    [[ -d $hour ]] || btrfs subvolume snapshot -r -- "/home" $hour
    [[ -d $day ]] || btrfs subvolume snapshot -r -- "/home" $day


    for f in {30..365}; do
        ago=$(date --date="$f days ago" -I)
        old="/home/.snapshots/$ago"
        [[ -d $old ]] && btrfs subvolume delete $old
    done
    exit 0
  '';
in
{
  systemd.timers = {
    "snap" = {
      wantedBy = [ "timers.target" ];
      enable = true;
      timerConfig = {
        OnBootSec = "1m";
        Unit = "home-snapshot.service";
        OnCalendar = "hourly";
      };
    };
  };

  systemd.services = {
    "home-snapshot" = {
      description = "take snapshot of home directory";
      serviceConfig.Type = "oneshot";
      path = [
        "/run/current-system/sw/"
        "/run/wrappers/"
      ];
      script = snapScript;
      wantedBy = [ "snap.timer" ];
    };

  };
}
