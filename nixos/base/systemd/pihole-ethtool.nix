{ config, pkgs, ... }:
let
  snapScript = ''
    #!/usr/bin/env zsh
    export PATH="/run/current-system/sw/bin/:/run/wrappers/bin/"
    NETDEV=$(ip -o route get 8.8.8.8 | cut -f 5 -d " ")
    sudo ethtool -K $NETDEV rx-udp-gro-forwarding on rx-gro-list off

  '';
in
{
  systemd.timers = {
    "ethtool" = {
      wantedBy = [ "timers.target" ];
      enable = true;
      timerConfig = {
        OnBootSec = "5m";
        Unit = "ethtool.service";
      };
    };
  };

  systemd.services = {
    "ethtool" = {
      description = "take snapshot of  directory";
      serviceConfig.Type = "oneshot";
      path = [
        "/run/current-system/sw/"
        "/run/wrappers/"
      ];
      script = snapScript;
      wantedBy = [ "ethtool.timer" ];
    };

  };
}
