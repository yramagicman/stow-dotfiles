{ config, pkgs, ... }:
let
  tailscaledns = ''
        #!/bin/sh
        set -eo pipefail
    /run/current-system/sw/bin/resolvectl dns tailscale0 100.94.223.34
  '';
in
{


  systemd.timers = {
    "tailscaledns" = {
      wantedBy = [ "timers.target" ];
      enable = true;
      timerConfig = {
        OnBootSec = "10s";
        Unit = "tailscaledns.service";
        OnCalendar = "hourly";
      };
    };
  };

  systemd.services = {
    "tailscaledns" = {
      description = "set tailscale dns correctly";
      wants = [ "tailscaled.service" "network-online.target" ];
      after = [ "tailscaled.service" "network-online.target" ];
      serviceConfig.Type = "oneshot";
      script = tailscaledns;
      wantedBy = [ "tailscaledns.timer" ];
    };

  };
}
