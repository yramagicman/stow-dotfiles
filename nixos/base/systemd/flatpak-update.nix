{ config, pkgs, ... }:
let
  update = ''
    #!/bin/sh
    set -eo pipefail
    export PATH="/run/current-system/sw/bin/:/run/wrappers/bin/"
    flatpak update --noninteractive --assumeyes
    flatpak uninstall --unused --assumeyes
    exit 0
  '';
in
{
  systemd.timers = {
    "flatpak-update" = {
      wantedBy = [ "timers.target" ];
      enable = true;
      timerConfig = {
        OnBootSec = "1hr";
        Unit = "flatpak-update.service";
        OnCalendar = "weekly";
        Persistent = true;
      };
    };
  };

  systemd.services = {
    "flatpak-update" = {
      description = "auto-update flatpaks";
      serviceConfig.Type = "oneshot";
      path = [
        "/run/current-system/sw/"
        "/run/wrappers/"
      ];
      script = update;
      wantedBy = [ "flatpak-update.timer" ];
    };

  };
}
