{ config, pkgs, ... }:
let
  snapScript = ''
    #!/usr/bin/env zsh
    export PATH="/run/current-system/sw/bin/:/run/wrappers/bin/"

    btrfs subvolume snapshot -r /srv/storage/nextcloud /srv/storage/backup/Nextcloud/$(date -I)

    for f in {30..365}; do
        ago=$(date --date="$f days ago" -I)
        old="/srv/storage/backup/Nextcloud/$ago"
        [[ -d $old ]] && btrfs subvolume delete $old
    done
    exit 0
  '';
in
{
  systemd.timers = {
    "nextcloud-snap" = {
      wantedBy = [ "timers.target" ];
      enable = true;
      timerConfig = {
        OnBootSec = "1m";
        Unit = "nextcloud-snapshot.service";
        OnCalendar = "daily";
      };
    };
  };

  systemd.services = {
    "nextcloud-snapshot" = {
      description = "take snapshot of nextcloud directory";
      serviceConfig.Type = "oneshot";
      path = [
        "/run/current-system/sw/"
        "/run/wrappers/"
      ];
      script = snapScript;
      wantedBy = [ "nextcloud-snap.timer" ];
    };

  };
}
