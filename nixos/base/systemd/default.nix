{ config, pkgs, ... }:
{
  imports = [
    ./balance.nix
    ./tailscaledns.nix
    ./scrub-status.nix
    ./scrub.nix
    ./snapshot.nix
    ./backup.nix
  ];
}
