{ config, pkgs, ... }:
let
  nix-channel = ''
    #!/bin/sh
    set -eo pipefail
    export PATH="/run/current-system/sw/bin/:/run/wrappers/bin/"
    nix-channel --update nixos
    exit 0
  '';
in
{
  systemd.timers = {

    "nix-channel" = {
      wantedBy = [ "timers.target" "network.target" ];
      enable = true;
      timerConfig = {
        Unit = "nix-channel.service";
        Persistent = true;
        OnCalendar = "daily";
      };
    };
  };


  systemd.services = {
    "nix-channel" = {
      serviceConfig.Type = "oneshot";
      description = "nix-channel update every day";
      script = nix-channel;
      wantedBy = [ "nix-channel.timer" ];
      after = [ "network-online.target" ];
      wants = [ "network-online.target" ];
    };
  };
}
