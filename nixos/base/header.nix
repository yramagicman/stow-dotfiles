# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:
{
  zramSwap.enable = true;

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.configurationLimit = 15;
  boot.loader.systemd-boot.enable = true;
  boot.loader.timeout = 1;
  boot.tmp.useTmpfs = true;
  boot.tmp.tmpfsSize = "25%";
  boot.tmp.cleanOnBoot = true;

  boot.kernel.sysctl = {
    "net.ipv6.conf.all.forwarding" = "1";
    "net.ipv4.ip_forward" = "1";
  };

  # Set your time zone.
  time.timeZone = "America/New_York";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";
  console = {
    keyMap = "us";
  };

  nix = {
    package = pkgs.nix;
    extraOptions = ''
      experimental-features = nix-command flakes auto-allocate-uids
      use-xdg-base-directories = true
      trusted-users = @wheel
    '';

    gc.automatic = true;
    gc.dates = "18:40";
    gc.options = "--delete-older-than 18d";
    settings.auto-optimise-store = true;
    optimise.automatic = true;
    optimise.dates = [ "11:50" "21:40" ];
  };

  system.stateVersion = "24.05";

  networking.hosts = {
    "100.94.223.34" = [ "browncoat" ];
    "192.168.1.224" = [ "browncoat.local" ];
    "100.87.66.73" = [ "kaylee-ts.local" ];
    "100.125.24.87" = [ "tightpants-ts.local" ];
    "100.78.151.73" = [ "the-yard" "the-yard.local" ];
    "192.168.1.100" = [ "kala" "kala.local" ];
  };
}
