{ config, pkgs, ... }:
{
  services.xserver = {
    enable = true;
    displayManager.gdm.enable = true;
    # desktopManager.gnome.enable = true;
    displayManager.gdm.autoSuspend = false;
    # windowManager.awesome.enable = true;
    windowManager.xmonad.enable = true;
    windowManager.xmonad.enableContribAndExtras = true;
  };

  programs.hyprland.enable = true;
  programs.waybar.enable = true;
  programs.hyprland.xwayland.enable = true;
  # services.displayManager.defaultSession = "none+xmonad";
  services.displayManager.defaultSession = "hyprland";
  # services.desktopManager.plasma6.enable = true;


  # Enable touchpad support (enabled default in most desktopManager).
  services.libinput.enable = true;
  services.libinput.touchpad.naturalScrolling = true;
  services.libinput.touchpad.disableWhileTyping = true;
  services.libinput.touchpad.accelSpeed = "0.25";
  services.libinput.mouse.naturalScrolling = true;
}
