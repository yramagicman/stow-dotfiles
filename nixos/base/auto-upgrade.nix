{ config, pkgs, ... }:
{

  system.autoUpgrade = {
    enable = true;
    dates = "weekly";
    flake = "git+https://gitlab.com/yramagicman/stow-dotfiles/";
    flags = [
      "--no-write-lock-file"
      "-L"
    ];
    allowReboot = true;
  };

}
