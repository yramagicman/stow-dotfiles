{ config, pkgs, ... }:
{
  imports = [
    ./auto-upgrade.nix
    ./doas.nix
    ./fonts.nix
    ./header.nix
    ./packages.nix
    ./podman.nix
    ./services.nix
    ./systemd
    ./users.nix
    ./xserver.nix
    ./yubikey.nix
  ];
}
