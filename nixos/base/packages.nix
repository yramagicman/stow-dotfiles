{ config, pkgs, ... }:
{
  nixpkgs.config.allowUnfree = true;
  programs.zsh.enable = true;
  programs.nix-ld.enable = true;
  programs.nix-ld.libraries = with pkgs; [

    # Add any missing dynamic libraries for unpackaged programs

    # here, NOT in environment.systemPackages

  ];

  environment.systemPackages = with pkgs; [
    btop
    # cachix
    cifs-utils
    cryptsetup
    ethtool
    file
    fio
    gcc
    git
    gnumake
    home-manager
    htop
    iperf
    kanata
    # killall
    libva
    lm_sensors
    lsof
    man-pages
    man-pages-posix
    mariadb
    neovim
    nfs-utils
    nix-index
    nmap
    nodejs
    nvme-cli
    openssl
    pinentry-gtk2
    pinentry-qt
    powertop
    python3Full
    racket
    rsync
    samba
    silver-searcher
    smartmontools
    tmux
    tree
    unzip
    w3m
    wget
    zip
    zsh
  ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  programs.gnupg.agent = {
    enable = true;
    enableSSHSupport = true;
  };
}
