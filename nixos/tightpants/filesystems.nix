{ anon, nfsopts, ... }: {

  fileSystems."/mnt/private" = {
    device = "100.94.223.34:srv/jonathan";
    fsType = "nfs";
    options = [ "${nfsopts}" ];
  };
  fileSystems."/home/jonathan/Storage" = {
    device = "//100.94.223.34/public";
    fsType = "smb3";
    options = [ "${anon}" ];
  };

  fileSystems."/home/jonathan/Videos" = {
    device = "//100.94.223.34/video";
    fsType = "smb3";
    options = [ "${anon}" ];
  };


  fileSystems."/home/jonathan/Music" = {
    device = "//100.94.223.34/music";
    fsType = "smb3";
    options = [ "${anon}" ];
  };

}
