{ authenticated, ... }:
{
  imports = [
    ./extras.nix
    ./hardware-configuration.nix
    ./network.nix
    ./filesystems.nix
  ];
}
