{ pkgs, ... }: {
  services.xserver.dpi = 192;
  hardware.acpilight.enable = true;
  hardware.cpu.intel.updateMicrocode = true;
  boot.kernelPackages = pkgs.linuxPackages_latest;

  environment.systemPackages = with pkgs; [
    intel-gpu-tools
    microcodeIntel
    solaar
  ];

  swapDevices = [
    {
      device = "/swapfile";
      size = 4096;
    }
  ];

  hardware.logitech.wireless.enable = true;
  hardware.logitech.wireless.enableGraphical = true;

  nixpkgs.config.packageOverrides = pkgs: {
    vaapiIntel = pkgs.vaapiIntel.override { enableHybridCodec = true; };
  };

  hardware.graphics = {
    enable = true;
    extraPackages = with pkgs; [
      intel-media-driver # LIBVA_DRIVER_NAME=iHD
      vpl-gpu-rt
      vaapiVdpau
      libvdpau-va-gl
    ];
  };
}
