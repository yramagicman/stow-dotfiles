{
  networking.hostName = "rivertam"; # Define your hostname.
  networking.wireless.iwd.enable = true; # Enables wireless support via iwd.

  networking.useDHCP = false;

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  systemd.network = {
    enable = true;
    networks = {
      "90-interfaces" = {
        matchConfig = { Name = "*"; };
        DHCP = "yes";
      };
    };
  };

  services.tailscale.port = 48616;
}
