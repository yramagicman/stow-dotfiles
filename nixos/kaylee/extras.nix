{ config, pkgs, nfsopts, ... }:
let
  sharedDirectories = [
    "Documents"
    "Calibre Library"
    "Pictures"
    "Videos"
    "Music"
    "Sites"
  ];
in
{

  environment.systemPackages = with pkgs; [
    amdgpu_top
  ];
  hardware.cpu.amd.updateMicrocode = true;
  # hardware.amdgpu.opencl.enable = true;
  hardware.amdgpu.amdvlk.enable = true;
  boot.kernelPackages = pkgs.linuxPackages_latest;
  services.xserver.videoDrivers = [ "amdgpu" ];
  # boot.extraModulePackages = with config.boot.kernelPackages; [ v4l2loopback.out ];

  # boot.kernelModules = [
  #   # Virtual Camera
  #   "v4l2loopback"
  #   # Virtual Microphone, built-in
  #   "snd-aloop"
  # ];

  # # Set initial kernel module settings
  # boot.extraModprobeConfig = ''
  #   # exclusive_caps: Skype, Zoom, Teams etc. will only show device when actually streaming
  #   # card_label: Name of virtual camera, how it'll show up in Skype, Zoom, Teams
  #   # https://github.com/umlaeute/v4l2loopback
  #   options v4l2loopback exclusive_caps=1 card_label="Virtual Camera"
  # '';

  fileSystems."/".options = [ "compress=zstd" ];
  services.xserver.dpi = 140;

  fileSystems."/home/jonathan/Storage" = {
    device = "100.94.223.34:srv/public";
    fsType = "nfs";
    options = [ "${nfsopts}" ];
  };

  fileSystems."/mnt/private" = {
    device = "100.94.223.34:srv/jonathan";
    fsType = "nfs";
    options = [ "${nfsopts}" ];
  };

  programs.dconf.enable = true;
  services.flatpak.enable = true;
  services.syncthing = {
    user = "jonathan";
    group = "users";
    enable = true;
    dataDir = "/home/syncthing";
    guiAddress = "0.0.0.0:8384";

    settings = {
      gui = {
        user = "jonathan";
        password = "syncmystuff";
      };

      devices = {
        "browncoat" = { id = "H2CBPQ3-VYQ7GUS-TFJEPMO-MBSUUI2-ACPLSCP-PLDH5IZ-P3XJN4B-HLPXMAE"; };
      };

      folders = builtins.listToAttrs (map
        (f:
          {
            name = "${f}";
            value = (builtins.listToAttrs [
              { name = "path"; value = "${config.users.users.jonathan.home}/${f}"; }
              { name = "devices"; value = [ "browncoat" ]; }
            ]);
          })
        sharedDirectories);
    };

  };

  networking.firewall.allowedTCPPorts = [
    8384
  ];

  hardware.graphics = {
    enable = true;
  };

  services.ollama = {
    enable = true;
    loadModels = [ "codellama" "qwen2.5-coder" "llama3.2" ];
    acceleration = "rocm";
    rocmOverrideGfx = "10.3.0";
  };

  # services.hardware.openrgb = {
  #   motherboard = "amd";
  #   enable = true;
  # };
}
