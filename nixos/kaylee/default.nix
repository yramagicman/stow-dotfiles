{ config, pkgs, ... }:
{
  imports = [
    ./extras.nix
    ./hardware-configuration.nix
    ./network.nix
    ../base/systemd/flatpak-update.nix
  ];
}
