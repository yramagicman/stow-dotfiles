{ config, pkgs, lib, ... }:
{
  home.packages = with pkgs; [
    asunder
    dbeaver-bin
    gphoto2
    # handbrake
    libsForQt5.kdenlive
    obs-studio
    xonotic
  ];
}
